# Glimpsee: Conception and testing of a personal assisting system


This is my **Final degree project**. I conceived and developed a **VR/AR** project with **Unity3D**, Vuforia and Google Cardboard. This project approaches an ideal personal assisting system named Glimpsee. It was presented in the Sonar Festival.

I also added the thesis' document at the root directory, named **Glimpsee.pdf**

* Demo Tic Tac Toe: <https://youtu.be/hnl--jLfEiY>
* Demo Test 1: <https://youtu.be/oauCiM5z4II>
* Demo Test 2: <https://youtu.be/QejmYgyGQ3U>
* Repository: <https://bitbucket.org/eduardtejerocirera/glimpsee/>