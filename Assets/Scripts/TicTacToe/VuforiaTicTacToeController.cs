﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class VuforiaTicTacToeController : MonoBehaviour {


	//private Looper m_looper;
	private List<GameObject> buttons;
	//private GameObject label;
	private Partida partida;
	private GlimpseeVoice gVoice;
	private VuforiaTicTacToe vTicTacToe;

	private bool partidaStarted = false;
	private bool isFirst = true;

	void Start () {


		partida = this.gameObject.GetComponent<Partida> ();
		//buttons = new List<GameObject>();

		//m_looper = SceneObjects.getInstance ().Looper;
		//m_looper.loadProgram (Looper.PGM_MARKER);
		//m_looper.loadProgram (Looper.PGM_MARKER_TICTACTOE);

		vTicTacToe = this.gameObject.GetComponent<VuforiaTicTacToe>();


		/*GameObject c = GameObject.Find ("Taulell").gameObject;
		for (int i = 0; i < c.transform.childCount; i++) {
			GameObject b = c.transform.GetChild (i).gameObject;
			buttons.Add (b);
			b.GetComponent<Button>().onClick.AddListener(delegate { onClick( int.Parse(b.name)); });
		}*/

		//label = GameObject.Find ("Label").gameObject;


		//GameObject.Find("Restart").GetComponent<Button>().onClick.AddListener(delegate { restart(); });
		gVoice = GameObject.Find ("GlimpseeVoice").GetComponent<GlimpseeVoice> ();
		gVoice.Load ();
		//gVoice.sayHello ();

	}

	public Partida getPartida(){
		return partida;
	}



	public void goStartPartida(){
		vTicTacToe.reset ();
		StartCoroutine ("startPartida");
	}

	/*private void restart(){
		//label.GetComponent<Text> ().text = "";
		//for (int i = 0; i < buttons.Count; i++) {
			//buttons [i].transform.GetChild (0).GetComponent<Text> ().text = "";
		//}
		StartCoroutine ("startPartida");
	}*/

	IEnumerator startPartida(){
		vTicTacToe.shouldCheck = false;

		if (isFirst) {
			gVoice.sayHello ();
			yield return new WaitForSeconds (gVoice.hello.length);
			yield return new WaitForSeconds (.4f);
			gVoice.sayLetsPlay ();

			yield return new WaitForSeconds (gVoice.letsPlay.length);
			yield return new WaitForSeconds (.4f);
		} else {
			gVoice.sayPlayAgain ();
			yield return new WaitForSeconds (gVoice.playAgain.length);
			yield return new WaitForSeconds (.4f);
		}

		int torn = partida.Init ();
		float whoStartsLength = 0;
		if (torn == partida.TORN_USER) {
			gVoice.sayUserStarts ();
			whoStartsLength = gVoice.userStarts.length;
		} else {
			gVoice.sayGlimpseeStarts ();
			whoStartsLength = gVoice.glimpseeStarts.length;
		}

		updateLabelTurn (torn);
		yield return new WaitForSeconds (whoStartsLength);
		yield return new WaitForSeconds (.4f);
		partidaStarted = true;
		isFirst = false;
		if (torn == partida.TORN_MACHINE)
			machineMoves ();
		vTicTacToe.shouldCheck = true;
	}

	public bool novaFitxa(int casella){
		if (partida.isGameOn && partida.currentTorn == partida.TORN_USER && partida.taulell.caselles [casella] == 0 ) {
			partida.taulell.caselles [casella] = partida.currentTorn;
			partida.currentTorn = partida.endTorn (partida.currentTorn);
			//gVoice.sayMin7 ();
			updateLabelTurn (partida.currentTorn);
			checkEndOfGame ();
			machineMoves ();
			return true;
		}
		return false;
	}

	private void machineMoves(){
		if (partidaStarted && partida.isGameOn && partida.currentTorn == partida.TORN_MACHINE) {
			vTicTacToe.shouldCheck = false ;
			int c = partida.move ();
			vTicTacToe.pieceToSquare (c);
			gVoice.sayMaj7 ();
			updateLabelTurn (partida.currentTorn);
			checkEndOfGame ();
			vTicTacToe.shouldCheck = true;
		}
	}

	private void checkEndOfGame(){
		if (partida.isGameOver (partida.taulell)) {
			partida.isGameOn = false;
			int score =  partida.analiseResult (partida.taulell, partida.TORN_USER);
			updatelabelWin (score);
		}
	}

	/*private void onClick(int casella){
		if (partida.isGameOn && partida.currentTorn == partida.TORN_USER && partida.taulell.caselles [casella] == 0 ) {
			partida.taulell.caselles [casella] = partida.currentTorn;
			marcaCasella (casella, partida.currentTorn);
			partida.currentTorn = partida.endTorn (partida.currentTorn);
			partida.checkEndOfGame ();
		}
	}*/



	/*public void marcaCasella(int casella, int id){
	string s = "";
	if (id == partida.TORN_USER) {
		s = "X";
	} else {
		s = "O";
	}

	buttons [casella].transform.GetChild (0).GetComponent<Text> ().text = s;
}*/

public void updateLabelTurn(int id){
	if (id == partida.TORN_USER){
		//label.GetComponent<Text>().text = "Your turn, user";
	}else{
		//label.GetComponent<Text>().text = "Machine's turn";
	}	
}


public void updatelabelWin(int score){
	switch (score) {
		case 10:
		//label.GetComponent<Text> ().text = "You win, user";
			gVoice.sayUserWin ();
			partida.DIFFICULTY++;
		break;
		case -10:
		//label.GetComponent<Text> ().text = "Machine wins";
			gVoice.sayGlimpseeWin ();
			if (partida.DIFFICULTY > 0)
				partida.DIFFICULTY--;
		break;
	case 0:
		//label.GetComponent<Text> ().text = "It's a Draw";
		gVoice.sayDraw ();
		break;
	}

}


}
