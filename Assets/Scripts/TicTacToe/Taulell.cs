﻿using UnityEngine;
using System.Collections;
using System;

public class Taulell
{
	private static int[] r1 = { 1, 1, 1, 0, 0, 0, 0, 0, 0 };
	private static int[] r2 = { 0, 0, 0, 1, 1, 1, 0, 0, 0 };
	private static int[] r3 = { 0, 0, 0, 0, 0, 0, 1, 1, 1 };
			 		
	private static int[] c1 = { 1, 0, 0, 1, 0, 0, 1, 0, 0 };
	private static int[] c2 = { 0, 1, 0, 0, 1, 0, 0, 1, 0 };
	private static int[] c3 = { 0, 0, 1, 0, 0, 1, 0, 0, 1 };
					 
	private static int[] d1 = { 1, 0, 0, 0, 1, 0, 0, 0, 1 };
	private static int[] d2 = { 0, 0, 1, 0, 1, 0, 1, 0, 0 };

	public int[] caselles;
	//public int casellesOcupades = 0;
	public int totalCaselles;

	public static int[][] winnings = {r1,r2,r3,c1,c2,c3,d1,d2};


	public Taulell(int gridSize){
		totalCaselles = gridSize * gridSize;
		caselles = new int[totalCaselles];
		totalCaselles = totalCaselles;
	}


	public Taulell(int[] NewCaselles){
		totalCaselles = NewCaselles.Length;
		caselles = new int[totalCaselles];
		copy (NewCaselles, caselles);
	}

	private void copy (int[]origin, int[]destination){
		for(int i = 0; i < origin.Length; i++){
			destination[i] = origin[i];
		}
	}

	/*public bool didWin(int id){

		for (int w = 0; w < winnings.Length; w++) {
			if (winnings [w] == multiplyAndNormalize(winnings[w], caselles))
				return true;
		}

		return false;
	}

	private int[] multiplyAndNormalize(int[] a, int[]b){
		int [] result = new int[totalCaselles];

		for (int i = 0; i < a.Length; i++) {
			result [i] = a [i] * b [i];
			if (result [i] == 2)
				result [i] = 1;
		}

		return result;
	}*/

	public int casellesOcupades(){
		int c = 0;
		for (int i = 0; i < totalCaselles; i++) {
			if (caselles [i] != 0)
				c++;
		}
		return c;

	}

	public bool ple(){
		return casellesOcupades() == totalCaselles;
	
	}
}

