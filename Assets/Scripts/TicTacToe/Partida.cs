﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Partida : MonoBehaviour
{

	public int TORN_USER = 1;
	public int TORN_MACHINE = 2;

	public int DIFFICULTY = 2; 

	public int gridSize = 3;
	public int currentTorn;
	public Taulell taulell;
	public bool isGameOn = false;
	public int[] cas = new int[9];

	private TictactoeController tController;


	/*void Start(){
		currentTorn = TORN_USER;
		taulell = new Taulell(new int[9]{2,0,1,1,0,0,1,2,2});
		move ();
		//checkEndOfGame ();
	}*/


	// Use this for initialization
	public int Init ()
	{
		taulell = new Taulell (gridSize);
		tController = this.gameObject.GetComponent<TictactoeController> ();
		isGameOn = true;

		currentTorn = whoStarts ();
		cas = taulell.caselles;
		return currentTorn;

		//tController.updateLabelTurn (currentTorn);
		//if (currentTorn != TORN_USER)
		//	move ();

		//taulell = new Taulell(new int[9]{1,0,1,2,1,1,2,0,2});
		/*currentTorn = TORN_USER;
		taulell = new Taulell(new int[9]{2,2,1,1,1,2,0,0,1});
		analiseResult (taulell, TORN_USER);*/
		/*currentTorn = TORN_USER;
		List<int> r = new List<int> ();
		int c = minMax (currentTorn, taulell, r);
		int final = findEmpty (taulell, r.IndexOf (c));
		taulell.caselles [final] = currentTorn;*/
	
	}


	/*void Update(){
		if (currentTorn == TORN_MACHINE && isGameOn) {
			move ();
		}
	}*/

	public int whoStarts(){
		int x = (int)Mathf.Round(Random.Range (0.0f, 1.0f));
		if (x == 0) {
			return TORN_USER;//user
		} else {
			return TORN_MACHINE;//machine
		}
	}

	public int move(){
		int final = 0;
		int rind = 0;
		int c = 0;
		List<int> r = new List<int> ();

		if (taulell.casellesOcupades() == 0 || DIFFICULTY == 0) {//primerTorn
			final = randomMove (taulell, currentTorn);
		} else {
			c = minimax (currentTorn, taulell, true, 0, r);
			rind = r.IndexOf (c);
			final = findEmpty (taulell, rind);
		}

		taulell.caselles [final] = currentTorn;
		currentTorn = endTorn(currentTorn);

		return final;

		//checkEndOfGame ();
			
			
	}
		
	/*public void checkEndOfGame(){
		if (isGameOver (taulell)) {
			isGameOn = false;
			int score =  analiseResult (taulell, TORN_USER);
			tController.updatelabelWin (score);
		}
	}*/

	public int endTorn(int torn){
		if (torn == TORN_MACHINE) {
			//tController.updateLabelTurn (TORN_USER);
			return TORN_USER;
		}
		if (torn == TORN_USER) {
			//tController.updateLabelTurn (TORN_MACHINE);
			return TORN_MACHINE;
		}
		return -1;
	}

	private int randomMove(Taulell t, int id){
		int numCasellesBuides = taulell.totalCaselles - taulell.casellesOcupades(); // numero de 0s, caselles buides
		int chosen = (int)Mathf.Round(Random.Range (0.0f, (float)numCasellesBuides - 1));//quina de les caselles buides omplirem

		return findEmpty (t,chosen);//return index de la chosen casella buida

	}
		

	private int minimax(int torn, Taulell t, bool isMax, int numMoves, List<int> results = null){

		if (isGameOver (t)) {
			int score = analiseResult (t, currentTorn);
			return score - numMoves;
		}

		if (numMoves == DIFFICULTY) {
			return 0;
		}
		//si s'ha arribat a la profunditat màxima es retorna un empat --> unknown result
		
		if (results == null)
			results = new List<int> ();
		
		int numCasellesBuides = t.totalCaselles - t.casellesOcupades();
		for (int i = 0; i < numCasellesBuides; i++) {
			int cOmplir = findEmpty (t, i);
			Taulell tAux = new Taulell (t.caselles);
			tAux.caselles [cOmplir] = torn;
			results.Add (minimax (tornComplementari(torn), tAux, !isMax, numMoves+1));
		}
		if (isMax) {
			return results.Max(); 
		} else {
			return results.Min(); 
		}
		// return max value
	}

	int tornComplementari(int torn){
		if (torn == TORN_USER)
			return TORN_MACHINE;
		return TORN_USER;
	}
		
	private int findEmpty(Taulell t, int emptyInd){
		int emptyCounter = 0;
		for (int i = 0; i < t.totalCaselles; i++) {
			if (t.caselles [i] == 0) {
				if (emptyCounter == emptyInd)
					return i; //index de la casella buida
				emptyCounter++;
			}
				
		}
		return -1;
	}


	public bool didWin(Taulell t, int id){

		for (int w = 0; w < Taulell.winnings.Length; w++) {
			if (equal(Taulell.winnings [w], multiplyAndNormalize(Taulell.winnings[w], t.caselles, id)) )
				return true;
		}

		return false;
	}

	private bool equal(int[]a, int[]b ){
		if (a.Length != b.Length)
			return false;
		for (int i = 0; i < a.Length; i++) {
			if (a [i] != b [i])
				return false;
		}
		return true;
	}


	public bool didAnyoneWin(Taulell t){
		return didWin (t, 1) || didWin (t, 2);
	}
		
	private int[] multiplyAndNormalize(int[] a, int[]b, int id){
		int [] result = new int[taulell.totalCaselles];

		for (int i = 0; i < a.Length; i++) {

			result [i] = a [i] * b [i]; //Multipliquem casella a casella

			if (result [i] != id)
				result [i] = 0; // netegem 

			if (id > 1 && result [i] > 1)
				result [i] = 1;
		}

		return result;
	}

	public bool isGameOver(Taulell t){
		return (t.casellesOcupades () == t.totalCaselles) || didAnyoneWin (t);
	}

	public int analiseResult(Taulell t, int torn){
		if (torn == TORN_MACHINE) {
			if (didWin (t, TORN_MACHINE)) {
				return 10;
			} else if (didWin (t, TORN_USER)) {
				return -10;
			} else {
				return 0;
			}
		} else {
			if (didWin (t, TORN_MACHINE)) {
				return -10;
			} else if (didWin (t, TORN_USER)) {
				return 10;
			} else {
				return 0;
			}
		}
	}

}

