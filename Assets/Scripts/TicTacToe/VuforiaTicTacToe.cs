﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;

public class VuforiaTicTacToe : MonoBehaviour
{
	public const byte CASELLA_0 = 1;
	public const byte CASELLA_1 = 2;
	public const byte CASELLA_2 = 3;
	public const byte CASELLA_3 = 4;
	public const byte CASELLA_4 = 5;
	public const byte CASELLA_5 = 6;
	public const byte CASELLA_6 = 7;
	public const byte CASELLA_7 = 8;
	public const byte CASELLA_8 = 9;

	public const byte FITXA_10 = 10;
	public const byte FITXA_11 = 11;
	public const byte FITXA_12 = 12;
	public const byte FITXA_13 = 13;
	public const byte FITXA_14 = 14;
	public const byte FITXA_15 = 15;
	public const byte FITXA_16 = 16;
	public const byte FITXA_17 = 17;
	public const byte FITXA_18 = 18;

	private byte[] emptyCaselles = {
		CASELLA_0,
		CASELLA_1,
		CASELLA_2,
		CASELLA_3,
		CASELLA_4,
		CASELLA_5,
		CASELLA_6,
		CASELLA_7,
		CASELLA_8
	};

	public const int STATE_CHANGE = 3;
	public const int MAX_ERRORS = 1;
	public bool shouldCheck = true;

	private byte[] currentCaselles = new byte[9];
	private byte[] tempCaselles = new byte[9];

	//bool[] foundCaselles = new bool[9]{ false, false, false, false, false, false, false, false, false };
	private VuforiaTicTacToeController vtController;


	//private MarkerDetection mDetection;
	public int changeStateCounter = 0;
	public int errorCounter = 0;
	// Use this for initialization

	private List<int> ids;
	private GameObject frameMarkers;
	void Start ()
	{
		//mDetection = SceneObjects.getInstance ().MarkerDetecScript;
		//mDetection = SceneObjects.getInstance ().MarkerDetecScript;
		frameMarkers = GameObject.Find("FrameMarkers").gameObject;
		vtController = this.gameObject.GetComponent<VuforiaTicTacToeController>();
		ids = new List<int> ();
	}



	void Update(){
		//if (vtController.getPartida().taulell != null)
		//	Debug.Log (vtController.getPartida ().taulell.caselles);

		if (shouldCheck){
			int num = howManyDetected();
			if ( num == 9) {
				lookForChanges (ids);
			} else if (changeStateCounter > 0){
				errorCounter++;
				if (errorCounter == MAX_ERRORS) {
					changeStateCounter = 0; //RESET, too many errors
					errorCounter = 0;
				}

			}
		}

	}

	private int howManyDetected(){

		StateManager sm = TrackerManager.Instance.GetStateManager ();

		ids.Clear ();

		// Query the StateManager to retrieve the list of
		// currently 'active' trackables 
		//(i.e. the ones currently being tracked by Vuforia)
		IEnumerable<TrackableBehaviour> activeTrackables = sm.GetActiveTrackableBehaviours ();
		foreach (TrackableBehaviour tb in activeTrackables) {
			MarkerBehaviour marker = (MarkerBehaviour) tb;
			ids.Add (marker.Marker.MarkerID);
		}

		return ids.Count;
	}

	private byte[] aux = new byte[9];
	private void lookForChanges(List<int> foundMarkers ){

		for(int i = 0; i < foundMarkers.Count; i++){
			aux [i] = (byte)foundMarkers [i];
		}

		fillOrderedIds (aux);

		if (changeStateCounter == 0 && !areEqual (currentCaselles, aux)) { //comparem amb les caselles actuals reals
			errorCounter = 0;
			changeStateCounter++; // primera vegada que entrem, actualitzem tempcaselles amb els 9 ids detectats.
			//Debug.Log ("Increment First");
			copy (aux, tempCaselles);
		} else if (changeStateCounter > 0 && areEqual (tempCaselles, aux)) {
			changeStateCounter++; // tornem a entrar, tempCaselles i aux han de ser iguals.
			if (changeStateCounter == STATE_CHANGE) {
				updateCurrentCaselles();// a les STATE_CHANGE coincidencies considerem que hi ha hagut un canvi i actualitzem.
				changeStateCounter = 0;
			}
		} else if(changeStateCounter > 0 && !areEqual(tempCaselles, aux)){
			errorCounter++;// no hem arribat a les 3 coincidencies i son diferents.
			if (errorCounter == MAX_ERRORS) {
				errorCounter = 0;
				changeStateCounter = 0;
			}
			//Debug.Log("Reset, are different");
		}
	}

	private void updateCurrentCaselles(){
		if (areEqual (tempCaselles, emptyCaselles)) {
			//Debug.Log ("START!!!!!!!!!");
			vtController.goStartPartida ();
			copy (emptyCaselles, currentCaselles);
		} else {
			//Debug.Log ("NEW FITXA!!!!!!!!!!");
			int c = getFitxaId ();
			if (c != -1) {
				if (vtController.novaFitxa (c)){
					copy (tempCaselles, currentCaselles); // nomes actualitzem currentCaselles si el moviment és vàlid
				}
			}
				
		}

		
	}

	private bool areEqual(byte[]a, byte[]b){
		for (int i = 0; i < a.Length; i++) {
			if (a [i] != b [i])
				return false;
		}

		return true;

	}

	private int getFitxaId(){
		for (int i = 0; i < tempCaselles.Length; i++) {
			if (tempCaselles [i] == FITXA_10 && currentCaselles [i] != FITXA_10)
				return i;
		}

		return -1;
	}

	private void copy(byte[]origin, byte[]dest){
		for (int i = 0; i < origin.Length; i++) {
			dest [i] = origin [i];
		}
	}

	/*
	private void emptyBoardDetected(){
		if (mDetection.howManyDetected () == 9) {
			checkDetected ();
		} else {
			okCounter = 0;
		}

		if (okCounter == 10) // taulell sencer vist durant 10 frames seguits.
			return true;

		return false;
	}*/

	private byte[]auxiliary = new byte[9];
	private void fillOrderedIds(byte[]a ){
		int fitxaCounter = 0;
		emptyArray (auxiliary);
		//Podem haver detectat més de 9 marcadors.
		for (int i = 0; i < a.Length; i++) {
			int casella = getIndexFromId (a [i], ref fitxaCounter);
			if (casella != -1) {
				//si no entra en algun id, aquella casella quedarà a 0
				auxiliary [casella] = a[i];
			}
		}

		copy (auxiliary, a);

		//omplim caselles a 0 amb tantes fitxes com hem detectat
		int c = 0;
		for (int j = 0; j < a.Length && c < fitxaCounter; j++) {
			if (a [j] == 0) {
				a [j] = FITXA_10;
				c++;
			}
		}
	} 

	/*private void resetFoundCaselles(){
		for (int i = 0; i < foundCaselles.Length; i++) {
			foundCaselles [i] = false;
		}	
	}*/

	private int getIndexFromId(byte id, ref int fitxaCounter){
		int casella = -1;

		switch (id) {
		case CASELLA_0:
			casella = 0;
			break;
		case CASELLA_1:
			casella = 1;
			break;
		case CASELLA_2:
			casella = 2;
			break;
		case CASELLA_3:
			casella = 3;
			break;
		case CASELLA_4:
			casella = 4;
			break;
		case CASELLA_5:
			casella = 5;
			break;
		case CASELLA_6:
			casella = 6;
			break;
		case CASELLA_7:
			casella = 7;
			break;
		case CASELLA_8:
			casella = 8;
			break;
		case FITXA_10:
		case FITXA_11:
		case FITXA_12:
		case FITXA_13:
		case FITXA_14:
		case FITXA_15:
		case FITXA_16:
		case FITXA_17:
		case FITXA_18:
			fitxaCounter++;
			break;
		default:
			break;
		}

		return casella;
	}

	public void pieceToSquare(int casella){
		Transform arObjsTransform = frameMarkers.transform.GetChild(casella);

		/*var children = new List<GameObject>();
		foreach (Transform child in arObjsTransform) 
			children.Add(child.gameObject);
			
		children.ForEach(child => Destroy(child));*/

		GameObject c = Instantiate (Resources.Load ("Prefabs/PieceGlimpsee"), new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
		c.transform.SetParent (arObjsTransform);
		c.transform.localPosition = new Vector3 (0f, 0f, 0f);
	}

	public void reset(){
		for (int i = 0; i < frameMarkers.transform.childCount; i++) { // eliminem ARObj FitxaGlimpsee
			Transform arObjsTransform = frameMarkers.transform.GetChild (i);

			var children = new List<GameObject>();
			foreach (Transform child in arObjsTransform) {
				if (child.name.Contains("PieceGlimpsee"))
					children.Add(child.gameObject);
			}


			children.ForEach(child => Destroy(child));
		}
	}

	private void emptyArray(byte[]a){
		for (int i = 0; i < a.Length; i++) {
			a [i] = 0;
		}
	}
}


