﻿using UnityEngine;
using System.Collections;

public class GlimpseeVoice : MonoBehaviour {

	private bool isEnglish;

	//TicTacToe
	public AudioClip hello;
	public AudioClip letsPlay;
	public AudioClip glimpseeStarts;
	public AudioClip userStarts;
	public AudioClip glimpseeTurn;
	public AudioClip userTurn;
	public AudioClip glimpseeWin;
	public AudioClip userWin;
	public AudioClip draw;
	public AudioClip playAgain;
	public AudioClip min7;
	public AudioClip maj7;

	public AudioClip[] letters;

	//Countdown ENGL
	public AudioClip getReady;
	public AudioClip three;
	public AudioClip two;
	public AudioClip one;
	public AudioClip go;
	public AudioClip getReadyHard;

	//Test1 SPANISH
	public AudioClip introTuto;
	public AudioClip preparate;
	public AudioClip tres;
	public AudioClip dos;
	public AudioClip uno;
	public AudioClip adelante;
	public AudioClip preparateHard;
	public AudioClip fintest1;
	public AudioClip fintuto;
	public AudioClip introL1;
	public AudioClip introt1L2;
	public AudioClip fint1L1;
	public AudioClip spiral;

	//Test2 SPANISH
	public AudioClip introEntrenamientoT2;
	public AudioClip finEntrenamientoT2;
	public AudioClip introL1T2;
	public AudioClip finL1T2;
	public AudioClip introL2T2;
	public AudioClip finT2;

	private AudioSource audioSource;

	public void Load (bool isEnglish = true) {
		audioSource = this.GetComponent<AudioSource> ();
		this.isEnglish = isEnglish;
	}

	public void sayLetter(char c){
		audioSource.clip = CentralData.getInstance().getVoiceLetterFromChar(c);
		audioSource.Play ();
	}

	public void saySpiral(){
		audioSource.clip = spiral;
		audioSource.Play ();
	}

	public void sayHello(){
		audioSource.clip = hello;
		audioSource.Play ();
	}

	public void sayLetsPlay(){
		audioSource.clip = letsPlay;
		audioSource.Play ();
	}

	public void sayGlimpseeStarts(){
		audioSource.clip = glimpseeStarts;
		audioSource.Play ();
	}

	public void sayUserStarts(){
		audioSource.clip = userStarts;
		audioSource.Play ();
	}

	public void sayGlimpseeTurn(){
		audioSource.clip = glimpseeTurn;
		audioSource.Play ();
	}

	public void sayUserTurn(){
		audioSource.clip = userTurn;
		audioSource.Play ();
	}

	public void sayGlimpseeWin(){
		audioSource.clip = glimpseeWin;
		audioSource.Play ();
	}	

	public void sayUserWin(){
		audioSource.clip = userWin;
		audioSource.Play ();
	}

	public void sayDraw(){
		audioSource.clip = draw;
		audioSource.Play ();
	}

	public void sayPlayAgain(){
		audioSource.clip = playAgain;
		audioSource.Play ();
	}

	public void sayMin7(){
		audioSource.clip = min7;
		audioSource.Play ();
	}

	public void sayMaj7(){
		audioSource.clip = maj7;
		audioSource.Play ();
	}



	//Countdown + TEST1

	public void sayIntroTutorial(){
		audioSource.clip = introTuto;
		audioSource.Play ();
	}

	public void sayIntroL1(){
		audioSource.clip = introL1;
		audioSource.Play ();
	}

	public void sayGetReady(){
		audioSource.clip = isEnglish ? getReady : preparate;
		audioSource.Play ();
	}

	public void sayThree(){
		audioSource.clip = isEnglish ? three : tres;
		audioSource.Play ();
	}

	public void sayTwo(){
		audioSource.clip = isEnglish ? two : dos;
		audioSource.Play ();
	}

	public void sayOne(){
		audioSource.clip = isEnglish ? one : uno;
		audioSource.Play ();
	}

	public void sayGo(){
		audioSource.clip = isEnglish ? go : adelante;
		audioSource.Play ();
	}

	public void sayGetReadyHard(){
		audioSource.clip = isEnglish ? getReadyHard : preparateHard;
		audioSource.Play ();
	}

	public void sayEndTutorial(){
		audioSource.clip = fintuto;
		audioSource.Play ();
	}

	public void sayEndTest(){
		audioSource.clip = fintest1;
		audioSource.Play ();
	}

	public void sayEndT1L1(){
		audioSource.clip = fint1L1;
		audioSource.Play ();
	}

	public void sayIntroT1L2(){
		audioSource.clip = introt1L2;
		audioSource.Play ();
	}

	//TEST2
	public void sayIntroTutorialT2(){
		audioSource.clip = introEntrenamientoT2;
		audioSource.Play ();
	}

	public void sayEndTutorialT2(){
		audioSource.clip = finEntrenamientoT2;
		audioSource.Play ();
	}

	public void sayIntroL1T2(){
		audioSource.clip = introL1T2;
		audioSource.Play ();
	}

	public void sayEndL1T2(){
		audioSource.clip = finL1T2;
		audioSource.Play ();
	}

	public void sayIntroL2T2(){
		audioSource.clip = introL2T2;
		audioSource.Play ();
	}

	public void sayEndTest2(){
		audioSource.clip = finT2;
		audioSource.Play ();
	}


}
