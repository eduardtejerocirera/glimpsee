﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class MyFrameMarker : MonoBehaviour
{

	public GameObject frameMarker;
	public Vector2 screenCoords;
	private int id;
	public string iconName;
	private HieroglyphVuforia hVuforia;

	public MyFrameMarker (GameObject f,Vector2 s, HieroglyphVuforia h){
		this.frameMarker = f;
		this.screenCoords = s;
		this.hVuforia = h;

		updateFrameMarkerData ();

	}

	public MyFrameMarker (HieroglyphVuforia h){
		frameMarker = new GameObject ();
		screenCoords = new Vector2 ();
		iconName = "";

		this.hVuforia = h;
	}

	private void updateFrameMarkerData(){
		id = frameMarker.GetComponent<MarkerAbstractBehaviour> ().Marker.MarkerID;
		iconName = hVuforia.getNameFromId (id);
	}

	public bool sameId(MyFrameMarker f){
		if (f.frameMarker.GetComponent<MarkerAbstractBehaviour>() != null)
			return f.frameMarker.GetComponent<MarkerAbstractBehaviour> ().Marker.MarkerID == id;

		return false;
	
	}

	public void copy(MyFrameMarker f){
		this.frameMarker = f.frameMarker;
		this.screenCoords = f.screenCoords;

		updateFrameMarkerData ();
	}

}

