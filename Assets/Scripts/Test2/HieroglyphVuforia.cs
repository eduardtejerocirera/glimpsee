﻿using UnityEngine;
using System.Collections;
using Vuforia;
using System.Collections.Generic;

public class HieroglyphVuforia : MonoBehaviour
{
	/*public const int CASELLA_0 = 1;
	public const int CASELLA_1 = 2;
	public const int CASELLA_2 = 3;
	public const int CASELLA_3 = 4;
	public const int CASELLA_4 = 5;
	public const int CASELLA_5 = 6;
	public const int CASELLA_6 = 7;
	public const int CASELLA_7 = 8;
	public const int CASELLA_8 = 9;*/


	public string[] icons;

	public int[] iconsIds;

	/*private byte[] emptyCaselles = {
		CASELLA_0,
		CASELLA_1,
		CASELLA_2,
		CASELLA_3,
		CASELLA_4,
		CASELLA_5,
		CASELLA_6,
		CASELLA_7,
		CASELLA_8
	};*/

	public const int STATE_CHANGE = 3;
	public bool shouldCheck = true;


	private HieroglyphController hController;


	//private MarkerDetection mDetection;
	public int changeStateCounter = 0;
	public int errorCounter = 0;

	private List<int> ids;
	private GameObject frameMarkers;
	public GameObject[] ARobjs;
	public MyFrameMarker[] currMarkers;
	private MyFrameMarker[] tempMarkers;
	private string[] newState;
	private GameObject[] newGO;
	//public GameObject[] currGameObjs;
	//public Vector2[] screenCoords;

	private int numSimultaneousFrameMarkers;
	private int numRows;
	private int numCols;

	private int[] orderedIds;

	public void Init (HieroglyphController hController)
	{

		iconsIds = new int[]{20,30,40,50,60,70};
		icons = new string[]{"smile", "lightning", "bug", "flower", "umbrella", "music"};

		this.hController = hController;
		ids = new List<int> ();

		numSimultaneousFrameMarkers = CentralData.getInstance ().numSquaresH;
		numCols = CentralData.getInstance ().numCh;
		numRows = CentralData.getInstance ().numRh;

		orderedIds = new int[numSimultaneousFrameMarkers];

		frameMarkers = GameObject.Find ("FrameMarkers").gameObject;
		int maxIconId = iconsIds [iconsIds.Length - 1];
		ARobjs = new GameObject[maxIconId+10];
		//currGameObjs = new GameObject[numSimultaneousFrameMarkers];
		//screenCoords = new Vector2[numSimultaneousFrameMarkers];
		currMarkers = new MyFrameMarker[numSimultaneousFrameMarkers];
		tempMarkers = new MyFrameMarker[numSimultaneousFrameMarkers];
		newGO = new GameObject[numSimultaneousFrameMarkers];
		for (int i = 0; i < tempMarkers.Length; i++) {
			tempMarkers [i] = new MyFrameMarker (this);
		}

		newState = new string[numSimultaneousFrameMarkers];


		//createFrameMarkers ();
		initFrameMarkers ();
		getARObjs ();
	}


	public bool detectInitialMarkers(){
		return howManyDetected () == numSimultaneousFrameMarkers;
	}


	/*private void createFrameMarkers(){
		for (int i = 0; i < frameMarkers.transform.childCount; i++) {
			GameObject o = (GameObject)Instantiate(Resources.Load("Prefabs/SquareH"));

			//Destroy (frameMarkers.transform.GetChild (i).GetChild(0));

			o.transform.parent = frameMarkers.transform.GetChild (i);
		}
	}*/

	private void initFrameMarkers(){
		int mId = 0;
		for (int i = 0; i < icons.Length; i++) {
			for (int j = 0; j < numSimultaneousFrameMarkers; j++) {
				mId = iconsIds [i] + j;
				GameObject o = GameObject.Find ("FrameMarker" + mId).gameObject;
				o.transform.GetChild (0).GetComponent<SquareH> ().Init ();
				o.transform.GetChild (0).GetComponent<SquareH> ().Tex = CentralData.getInstance ().getIconFromName (icons [i]);
			}
		}
	}

	private void getARObjs(){
		for(int i = 0; i < ARobjs.Length; i++){
			ARobjs[i] = frameMarkers.transform.GetChild (i).gameObject;
		}
	}



	public void detectMarkers(){
		if (howManyDetected () == numSimultaneousFrameMarkers) {
			//lookForChanges (ids);
			whichARObjs(ids);
			updateStates ();
		}
	}

	private void updateStates(){
		if (areEqual (currMarkers, tempMarkers) && changeStateCounter < STATE_CHANGE) {
			changeStateCounter++;
			if (changeStateCounter == STATE_CHANGE) {
				buildNewState ();
				hController.newState (newState, newGO);
			}
				
		} else if (!areEqual (currMarkers, tempMarkers)){
			copy (currMarkers, tempMarkers);
			changeStateCounter = 0;
		}
			
	
	}

	private void buildNewState(){
		for (int i = 0; i < numSimultaneousFrameMarkers; i++) {
			newState [i] = tempMarkers [i].iconName;
			newGO [i] = tempMarkers [i].frameMarker.transform.GetChild(0).gameObject;//Squares
			newGO[i].GetComponent<SquareH>().Init();
		}
	}

	private int howManyDetected(){

		StateManager sm = TrackerManager.Instance.GetStateManager ();

		ids.Clear ();

		// Query the StateManager to retrieve the list of
		// currently 'active' trackables 
		//(i.e. the ones currently being tracked by Vuforia)
		IEnumerable<TrackableBehaviour> activeTrackables = sm.GetActiveTrackableBehaviours ();
		foreach (TrackableBehaviour tb in activeTrackables) {
			MarkerBehaviour marker = (MarkerBehaviour) tb;
			ids.Add (marker.Marker.MarkerID);
		}

		return ids.Count;
	}

	private void whichARObjs(List<int> ids){
		for (int i = 0; i < ids.Count; i++) {
			//currGameObjs [i] = ARobjs [ids [i]];
			//screenCoords[i] = extractScreenCoordinates(ARobjs [ids [i]]);
			int id = ids[i];
			currMarkers[i] = new MyFrameMarker( ARobjs[id], 
				extractScreenCoordinates(ARobjs[ids[i]]), this );

		}

		orderObjsByYCoordinates ();

		for (int i = 0; i < numRows; i++) {
			orderObjsByXCoordinates (i);
		}
			

	}

	private Vector2 extractScreenCoordinates(GameObject o){

		//float targetSize = o.GetComponent<MarkerAbstractBehaviour>().Marker.GetSize();
		//float targetAspect = targetSize / targetSize; //it's a square

		// We define a point in the target local reference 
		// we take the bottom-left corner of the target, 
		// just as an example
		// Note: the target reference plane in Unity is X-Z, 
		// while Y is the normal direction to the target plane
		//Vector3 pointOnTarget = new Vector3(-0.5f, 0, -0.5f/targetAspect); 
		Vector3 pointOnTarget = o.transform.position;

		// We convert the local point to world coordinates
		Vector3 targetPointInWorldRef = transform.TransformPoint(pointOnTarget);

		// We project the world coordinates to screen coords (pixels)
		Vector3 screenPoint = Camera.main.WorldToScreenPoint(targetPointInWorldRef);

		//Debug.Log ("target point in screen coords: " + screenPoint.x + ", " + screenPoint.y);
		return new Vector2 (screenPoint.x, screenPoint.y);

	}

	private void orderObjsByYCoordinates(){
		MyFrameMarker aux;
		for (int i = 1; i < currMarkers.Length; i++) {
			for (int j = 0; j < currMarkers.Length - 1; j++) {
				if (currMarkers[j].screenCoords.y < currMarkers[j+1].screenCoords.y) {
					aux = currMarkers [j];
					currMarkers [j] = currMarkers [j + 1];
					currMarkers [j + 1] = aux;
				}
			}
		}
	}

	private void orderObjsByXCoordinates(int r){
		MyFrameMarker aux;
		int c = r * numCols;
		for (int i = 1; i < numCols; i++) {
			for (int j = 0; j < numCols - 1; j++) {
				if (currMarkers[c+j].screenCoords.x > currMarkers[c+j+1].screenCoords.x) {
					aux = currMarkers [c+j];
					currMarkers [c+j] = currMarkers [c+j + 1];
					currMarkers [c+j + 1] = aux;
				}
			}
		}
	}


	private void lookForChanges(List<int> foundMarkers){
		fillOrderedIds (foundMarkers, orderedIds);
	}

	private byte[]auxiliary = new byte[9];
	private void fillOrderedIds(List<int> foundMarkers, int[] orderedIds ){
		emptyArray (orderedIds);
		for (int i = 0; i < numSimultaneousFrameMarkers; i++) {
			int ind = getIndexFromId (foundMarkers [i]);
			if (ind != -1)
				orderedIds[ind] = foundMarkers [i];
		}


		/*int fitxaCounter = 0;
		emptyArray (auxiliary);
		//Podem haver detectat més de 9 marcadors.
		for (int i = 0; i < a.Length; i++) {
			int casella = getIndexFromId (a [i], ref fitxaCounter);
			if (casella != -1) {
				//si no entra en algun id, aquella casella quedarà a 0
				auxiliary [casella] = a[i];
			}
		}

		copy (auxiliary, a);

		//omplim caselles a 0 amb tantes fitxes com hem detectat
		int c = 0;
		for (int j = 0; j < a.Length && c < fitxaCounter; j++) {
			if (a [j] == 0) {
				a [j] = FITXA_10;
				c++;
			}
		}*/
	} 

	public string getNameFromId(int id){
		int n = id / 10; // 26/10 = 2
		n = n * 10; // 2 * 10 = 20

		for (int i = 0; i < iconsIds.Length; i++) {
			if (iconsIds [i] == n)
				return icons[i];
		}

		return "";
	}


	public int getIndexFromId(int id){
		if (id < numSimultaneousFrameMarkers) 
			return id;
		return -1;
		//int n = id / 10; //26/10 = 2
		//n = n * 10; //2 * 10 = 20
	}

	/*private byte[] aux = new byte[9];
	private void lookForChanges(List<int> foundMarkers ){

		for(int i = 0; i < foundMarkers.Count; i++){
			aux [i] = (byte)foundMarkers [i];
		}

		fillOrderedIds (aux);

		if (changeStateCounter == 0 && !areEqual (currentCaselles, aux)) { //comparem amb les caselles actuals reals
			errorCounter = 0;
			changeStateCounter++; // primera vegada que entrem, actualitzem tempcaselles amb els 9 ids detectats.
			//Debug.Log ("Increment First");
			copy (aux, tempCaselles);
		} else if (changeStateCounter > 0 && areEqual (tempCaselles, aux)) {
			changeStateCounter++; // tornem a entrar, tempCaselles i aux han de ser iguals.
			if (changeStateCounter == STATE_CHANGE) {
				updateCurrentCaselles();// a les STATE_CHANGE coincidencies considerem que hi ha hagut un canvi i actualitzem.
				changeStateCounter = 0;
			}
		} else if(changeStateCounter > 0 && !areEqual(tempCaselles, aux)){
			errorCounter++;// no hem arribat a les 3 coincidencies i son diferents.
			if (errorCounter == MAX_ERRORS) {
				errorCounter = 0;
				changeStateCounter = 0;
			}
			//Debug.Log("Reset, are different");
		}
	}*/

	/*private void updateCurrentCaselles(){
		if (areEqual (tempCaselles, emptyCaselles)) {
			//Debug.Log ("START!!!!!!!!!");
			vtController.goStartPartida ();
			copy (emptyCaselles, currentCaselles);
		} else {
			//Debug.Log ("NEW FITXA!!!!!!!!!!");
			int c = getFitxaId ();
			if (c != -1) {
				if (vtController.novaFitxa (c)){
					copy (tempCaselles, currentCaselles); // nomes actualitzem currentCaselles si el moviment és vàlid
				}
			}

		}


	}*/


	private bool areEqual(MyFrameMarker[]a, MyFrameMarker[]b){
		for (int i = 0; i < a.Length; i++) {
		if (!a[i].sameId(b[i]))
				return false;
		}

		return true;

	}


	private void copy(MyFrameMarker[]origin, MyFrameMarker[]dest){
		for (int i = 0; i < origin.Length; i++) {
			dest [i].copy (origin [i]);
		}
	}
	

	/*private int getIndexFromId(byte id, ref int fitxaCounter){
		int casella = -1;

		switch (id) {
		case CASELLA_0:
			casella = 0;
			break;
		case CASELLA_1:
			casella = 1;
			break;
		case CASELLA_2:
			casella = 2;
			break;
		case CASELLA_3:
			casella = 3;
			break;
		case CASELLA_4:
			casella = 4;
			break;
		case CASELLA_5:
			casella = 5;
			break;
		case CASELLA_6:
			casella = 6;
			break;
		case CASELLA_7:
			casella = 7;
			break;
		case CASELLA_8:
			casella = 8;
			break;
		case FITXA_10:
		case FITXA_11:
		case FITXA_12:
		case FITXA_13:
		case FITXA_14:
		case FITXA_15:
		case FITXA_16:
		case FITXA_17:
		case FITXA_18:
			fitxaCounter++;
			break;
		default:
			break;
		}

		return casella;
	}*/

	public void pieceToSquare(int casella){
		Transform arObjsTransform = frameMarkers.transform.GetChild(casella);

		/*var children = new List<GameObject>();
	foreach (Transform child in arObjsTransform) 
		children.Add(child.gameObject);
		
	children.ForEach(child => Destroy(child));*/

		GameObject c = Instantiate (Resources.Load ("Prefabs/PieceGlimpsee"), new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
		c.transform.SetParent (arObjsTransform);
		c.transform.localPosition = new Vector3 (0f, 0f, 0f);
	}

	public void reset(){
		for (int i = 0; i < frameMarkers.transform.childCount; i++) { // eliminem ARObj FitxaGlimpsee
			Transform arObjsTransform = frameMarkers.transform.GetChild (i);

			var children = new List<GameObject>();
			foreach (Transform child in arObjsTransform) {
				if (child.name.Contains("PieceGlimpsee"))
					children.Add(child.gameObject);
			}


			children.ForEach(child => Destroy(child));
		}
	}

	private void emptyArray(int[]a){
		for (int i = 0; i < a.Length; i++) {
			a [i] = 0;
		}
	}
		
}

