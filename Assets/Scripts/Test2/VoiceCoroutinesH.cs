﻿using UnityEngine;
using System.Collections;

public class VoiceCoroutinesH : MonoBehaviour
{

	private HieroglyphController hController;
	public GlimpseeVoice gVoice;

	public void Init (HieroglyphController hController) {
		gVoice = GameObject.Find ("GlimpseeVoice").GetComponent<GlimpseeVoice> ();
		gVoice.Load (false);

		this.hController = hController;
	}

	public void stopAll(){
		StopAllCoroutines ();
	}
		
	public void t2_startCountdown(int level){
		StartCoroutine (t2_countDown(level));
	}

	IEnumerator t2_countDown(int level){
		if (!CentralData.getInstance().debug) {
			gVoice.sayGetReady ();
			yield return new WaitForSeconds (2);

			gVoice.sayThree ();
			yield return new WaitForSeconds (1);
			gVoice.sayTwo ();
			yield return new WaitForSeconds (1);
			gVoice.sayOne ();
			yield return new WaitForSeconds (1);
		}
		gVoice.sayGo ();
		yield return new WaitForSeconds (1f);

		hController.initAndStart ();
	}

	public void t2_sayIntro(int level){
		StartCoroutine (sayIntro (level));
	}

	IEnumerator sayIntro(int level){
		if (!CentralData.getInstance ().debug) {
			switch (level) {
			case 0:
				gVoice.sayIntroTutorialT2 ();
				yield return new WaitForSeconds (10f);
				break;
			case 1:
				gVoice.sayIntroL1T2 ();
				yield return new WaitForSeconds (8f);
				break;
			case 2:
				gVoice.sayIntroL2T2 ();
				yield return new WaitForSeconds (5f);
				break;
			}
		}
			
		yield return 0;
		t2_startCountdown (level);

	}

	public void t2_sayEnd(int level, bool gameOver){
		StartCoroutine (sayEnd (level, gameOver));
	}

	IEnumerator sayEnd(int level, bool gameOver){
		switch (level) {
		case 0:
			gVoice.sayEndTutorialT2 ();
			yield return new WaitForSeconds (2f);
			break;
		case 1:
			gVoice.sayEndL1T2 ();
			yield return new WaitForSeconds (4.5f);
			break;
		case 2:
			gVoice.sayEndL1T2 ();
			yield return new WaitForSeconds (4.5f);
			gVoice.sayEndTest2 ();
			yield return new WaitForSeconds (1.5f);
			break;
		}

		if(!gameOver)
			t2_sayIntro (level+1);

	}
		
}
	

