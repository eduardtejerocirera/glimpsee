﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class HieroglyphController : MonoBehaviour {

	private HieroglyphLogic hLogic;
	private GameObject canvas;

	//private List<GameObject> buttons = new List<GameObject>();

	//private Color HIGHLIGHT = Color.blue;
	//private Color STANDARD = Color.white;

	private bool gameOn = true;

	private float[][] hTimes = new float[][]{
		new float[]{
			-1f, -1f, -1f, 
			-1f, -1f, -1f,
			-1f, -1f, -1f},
		new float[]{
			2.0f, 2.0f, 2.0f, 
			2.0f, 2.0f, 2.0f,
			2.0f, 2.0f, 2.0f},
		new float[]{
			1.0f, 1.0f, 1.0f, 
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f},
	};

	private float[][] blinkingTimes = new float[][]{
		new float[]{
			-1f, -1f, -1f, 
			-1f, -1f, -1f,
			-1f, -1f, -1f},
		new float[]{
			2.0f, 2.0f, 2.0f, 
			2.0f, 2.0f, 2.0f,
			2.0f, 2.0f, 2.0f},
		new float[]{
			1.0f, 1.0f, 1.0f, 
			1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, 1.0f},
	};

	private int[] howManyBlinks = new int[]{-1,10,4};
		
	public VoiceCoroutinesH voiceCH;

	private int numSquares;
	private GameObject[] squares;

	private HieroglyphVuforia hVuforia;

	// Use this for initialization
	void Start () {
		hLogic = this.GetComponent<HieroglyphLogic> ();
		hLogic.Create (CentralData.getInstance().hieroglyphLevel);

		hVuforia = this.GetComponent<HieroglyphVuforia> ();
		hVuforia.Init (this);

		voiceCH = this.GetComponent<VoiceCoroutinesH> ();
		voiceCH.Init (this);


		numSquares = CentralData.getInstance ().numSquaresH;

		squares = new GameObject[numSquares];

		StartCoroutine (detectInitialMarkers ());

		CentralData.CheatingEvent += cheatingListener;
	}

	IEnumerator detectInitialMarkers(){
		while (!hVuforia.detectInitialMarkers ()) {
			yield return 0;
		}

		Init ();
	}

	public void Init(){
		for (int i = 0; i < squares.Length; i++) {
			int j = i + 1;
			squares [i] = GameObject.Find ("FrameMarker" + j).transform.GetChild(0).gameObject;
			squares [i].GetComponent<SquareH> ().Init ();
		}

		voiceCH.t2_sayIntro (hLogic.level);
	}

	public void initAndStart(){
		gameOn = true;

		hLogic.Init ();
		initTextures ();
		resetSquares ();
		squares [0].GetComponent<SquareH> ().makeCurrent();
		StartCoroutine (mainLoop ());
	}

	private void resetSquares(){
		for (int i = 0; i < squares.Length; i++) {
			squares [i].GetComponent<SquareH> ().makeNothing ();
		}
	}

	private void initTextures(){
		char[] dict = hLogic.getSquares();
		for (int i = 0; i < squares.Length; i++) {
			string texName = squares [i].GetComponent<SquareH>().Tex.name;
			char c = dict [i];

			Texture2D img = CentralData.getInstance ().getTextureLetter (char.ToUpper(c));
			squares [i].GetComponent<SquareH>().Tex = img;

			string iconName = hLogic.getIconFromInd (i);
			squares [i].GetComponent<SquareH> ().iconName = iconName;
			squares [i].GetComponent<SquareH> ().letter = c;

		}
	}

	IEnumerator mainLoop(){
		while(gameOn){
			hVuforia.detectMarkers (); //when new piece detected, hVuforia calls newState
			if (hLogic.level > 0 && gameOn) {
				int l = hLogic.level;
				int ind = hLogic.currChar;
				squares [ind].GetComponent<SquareH> ().startCheckForHelper (hTimes [l] [ind], 
					blinkingTimes[l][ind], howManyBlinks[l], hLogic);
			}
			yield return 0;
		}
	}

	/*IEnumerator checkHelpers(){
		int aux = hLogic.currChar;
		int l = hLogic.level;
		string ic = hLogic.getCurrCharIcon ();
		Texture2D iconTex = CentralData.getInstance ().getIconFromName (ic);
		Texture2D originalTex = squares [aux].GetComponent<SquareH> ().Tex;
		float blinkingTime = 2.0f;
		bool blinkOn = false;
		int counter = 0;

		yield return new WaitForSeconds (hTimes [l][aux]);

		while (aux == hLogic.currChar && l == hLogic.level) {
			counter++;
			counter = counter % 4;

			blinkOn = !blinkOn;
			if (blinkOn) {
				squares [aux].GetComponent<SquareH> ().Tex = iconTex;
			} else {
				squares [aux].GetComponent<SquareH> ().Tex = originalTex;
			}

			yield return new WaitForSeconds (blinkingTime);

			if (counter == 0)
				blinkingTime = blinkingTime * 0.5f;
		}

		squares [aux].GetComponent<SquareH> ().Tex = originalTex;		
	}*/

	public void newState(string[] newState, GameObject[] arr){
		for (int i = 0; i < newState.Length; i++) {

			if (hLogic.isNew (newState [i], i)) {//isNew updates currState with corresponding char
				if (newState [i] != "") {
					checkRightOrWrong (i, arr);
				}

			}
		}
	}

	private void checkRightOrWrong(int i, GameObject[] arr){
		if (hLogic.checkCharAt (i)) {
			StartCoroutine (makeRight (arr [i].GetComponent<SquareH> ()));
			checkEnd ();
		} else {
			arr [i].GetComponent<SquareH> ().makeWrong ();
		}
	}

	private void checkEnd(){
		if (hLogic.levelCompleted ()) {
			updateLevel ();
		} else {
			int x = hLogic.updateCurrentChar ();
			squares [x].GetComponent<SquareH> ().makeCurrent();
			if (x > 0)
				squares [x - 1].GetComponent<SquareH> ().makeNothing ();
		}
	}


	IEnumerator makeRight(SquareH s){
		s.makeRight ();
		yield return new WaitForSeconds (1.8f);
		s.makeNothing ();
	}

	public void updateLevel(){
		gameOn = false;
		voiceCH.t2_sayEnd (hLogic.level, hLogic.gameIsOver ()); //checks end of game
		hLogic.level++;
	}

	private void cheatingListener(){
		voiceCH.stopAll ();
		updateLevel ();
	}

	

}
