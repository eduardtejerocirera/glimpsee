﻿using UnityEngine;
using System.Collections;

public class HieroglyphLogic : MonoBehaviour {


	/*private string[] distractorNames = new string[72]{
		"cube","anarchy","clover","batman","mickey","sword","ying","alpha","music","glasses",
		"fish","pi","peace","umbrella","bird","trophy","trash","drop","clock","phone",
		"sun","star","smile","shop","crosshair","wifi","refresh","question","puzzle","power",
		"plane","clip","lock","headphones","cross","micro","man","loupe","lightning","bulb",
		"lemon","home","heart","game","cloud","eye","euro","earth","envelope","dropbox",
		"ball","dollar","scissors","comment","engine","coffee","circle","bullseye","bug","bell",
		"ban","apple","flower","&","@","%","battery","diamond","car","key",
		"eject","#"};*/

	private char[][] dictionary = new char[][]{
		new char[]{'A', 'E','A','E','A','E','A','E','A'},
		new char[]{'Z','E','N', 'S','A','L', 'L', 'E','S' },
		new char[]{'Z','A','S', 'S','A','N', 'L', 'E','E' }
	};

	private char[] letters = new char[]{
		'A','E','Z','N','S','L'
	};

	private string[] icons = new string[]{
		//"smile", "bug", "bell", "flower", "cloud"
		"smile","lightning","bug","flower","umbrella","music"
	};



	//public char[] squares = null;
	private char[] currState;


	public int numLetters = 9;
	//public string phrase = "hola bola";
	public int currChar = 0;

	//private string remainingWord = "";
	public int level = 0;

	public void Create(int l){
		level = l;
		if (currState == null) {
			//squares = new char[CentralData.getInstance ().numSquaresH];
			currState = new char[CentralData.getInstance ().numSquaresH]; 
		}		
	}


	public void Init(){
		currChar = 0;

		for (int i = 0; i < currState.Length; i++) {
			currState [i] = ' ';
		}
	}

	public char[] getSquares(){
		return dictionary[level];
	}

	/*public bool checkChar(string s, int ind){
		char c = findCorrespondingLetter (s);
		return c == squares [ind];
	}*/

	private char findCorrespondingLetter(string s){
		if (s == "")
			return ' ';

		for (int i = 0; i < icons.Length; i++) {
			if (icons[i] == s)
				return letters[i];
		}
		return ' ';
	}

	public int updateCurrentChar(){
		for (int i = 0; i < currState.Length; i++) {
			if(currState[i] != dictionary[level][i]){
				currChar = i;
				return currChar;
			}
		}

		return -1;
	}

	public string getIconFromInd(int ind){
		return findCorrespondingIcon (dictionary [level] [ind]);
	
	}

	public bool letterIsEasy(char c){
		for (int i = 0; i < dictionary [0].Length; i++) {
			if (c == dictionary [0] [i])
				return true;
		}

		return false;
	}


	public string getCurrCharIcon(){
		return findCorrespondingIcon(currState[currChar]);
	}

	private string findCorrespondingIcon(char c){
		for (int i = 0; i < letters.Length; i++) {
			if (letters [i] == c)
				return icons [i];
		}

		return "";
	}

	public void checkNewState(string[] newState){
		char c;
		for (int i = 0; i < newState.Length; i++) {
			c = findCorrespondingLetter (newState [i]);
			if (c != currState[i]) {
					currState[i] = c;

			}
		}
	}

	public bool isNew(string s, int ind){
		char c = findCorrespondingLetter (s);
		if (currState [ind] != c) {
			currState [ind] = c;
			return true;
		}
		return false;
	}

	public bool checkCharAt(int ind){
		return currState [ind] == dictionary[level] [ind];
	}

	public bool gameIsOver(){
		return level >= dictionary.Length - 1;
	}

	public bool levelCompleted(){
		for (int i = 0; i < currState.Length; i++) {
			if (currState [i] != dictionary [level] [i])
				return false;
		}
		return true;
	}
	
}
