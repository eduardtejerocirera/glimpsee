﻿using UnityEngine;
using System.Collections;

public class SquareH : MonoBehaviour {

	private GameObject wrongCube;
	private GameObject rightCube;
	private GameObject currCube;
	public string iconName;
	public char letter;

	public AudioClip correct;
	public AudioClip wrong;
	public AudioClip clue;

	private AudioSource audioSource;

	private bool activeHelpCoroutine = false;


	private Texture2D tex;
	public Texture2D Tex{
		get{ 
			return tex;
		}
		set{ 
			tex = value;
			this.transform.GetChild (0).GetComponent<Renderer> ().material.mainTexture = tex;
		}

	}

	private bool inited = false;

	public void Init () {
		if (!inited) {
			if (tex == null)
				tex = (Texture2D)this.transform.GetChild (0).GetComponent<Renderer> ().material.mainTexture;

			if (currCube == null) {
				currCube = this.transform.GetChild (1).gameObject;
				rightCube = this.transform.GetChild (2).gameObject;
				wrongCube = this.transform.GetChild (3).gameObject;
			}

			currCube.SetActive (false);
			rightCube.SetActive (false);
			wrongCube.SetActive (false);

			audioSource = this.GetComponent<AudioSource> ();

			inited = true;
		}

	}

	public void makeCurrent(){
		currCube.SetActive (true);
		rightCube.SetActive (false);
		wrongCube.SetActive (false);
	}

	public void makeRight(){
		rightCube.SetActive (true);
		currCube.SetActive (false);
		wrongCube.SetActive (false);
		StartCoroutine(playRight ());
	}

	public void makeWrong(){
		wrongCube.SetActive (true);
		currCube.SetActive (false);
		rightCube.SetActive (false);
		StartCoroutine(playWrong ());
	}	

	public void makeNothing(){
		currCube.SetActive (false);
		rightCube.SetActive (false);
		wrongCube.SetActive (false);
	}

	IEnumerator playRight(){
		float t = Mathf.Round(Random.Range (0.0f, (float)0.15f));
		yield return new WaitForSeconds(t);

		audioSource.clip = correct;
		audioSource.volume = 0.5f;
		audioSource.Play ();
	}

	IEnumerator playWrong(){
		float t = Mathf.Round(Random.Range (0.0f, (float)0.15f));
		yield return new WaitForSeconds(t);

		audioSource.clip = wrong;
		audioSource.volume = 0.5f;
		audioSource.Play ();
	}

	IEnumerator playClue(){
		float t = Mathf.Round(Random.Range (0.0f, (float)0.15f));
		yield return new WaitForSeconds(t);

		audioSource.clip = clue;
		audioSource.volume = 1.0f;
		audioSource.Play ();
	}

	public void startCheckForHelper(float waitTime, float blinkingTime, int counterMax, HieroglyphLogic hLogic){
		if (!activeHelpCoroutine)
			StartCoroutine (checkForHelper(waitTime, blinkingTime, counterMax, hLogic));
	}

	IEnumerator checkForHelper(float waitTime, float blinkingTime,int counterMax, HieroglyphLogic hLogic){
		activeHelpCoroutine = true;

		int aux = hLogic.currChar;
		int l = hLogic.level;
		string ic = hLogic.getCurrCharIcon ();
		bool blinkOn = false;
		int counter = 0;

		Texture2D iconTex = CentralData.getInstance().getIconFromName(iconName);
		Texture2D originalTex = CentralData.getInstance ().getTextureLetter (letter);

		//if (hLogic.level == 1 && hLogic.letterIsEasy (letter))
		//	waitTime = waitTime * 5;

		yield return new WaitForSeconds (waitTime);

		while (aux == hLogic.currChar && l == hLogic.level) {
			counter++;
			counter = counter % counterMax;

			blinkOn = !blinkOn;
			if (blinkOn) {
				this.Tex = iconTex;
			} else {
				this.Tex = originalTex;
			}

			StartCoroutine(playClue ());

			yield return new WaitForSeconds (blinkingTime);

			if (counter == 0 && blinkingTime > 0.25f)
				blinkingTime = blinkingTime * 0.5f;
		}

		this.Tex = tex;
		activeHelpCoroutine = false;
		
	}

}
