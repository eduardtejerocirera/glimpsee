﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

using OpenCVForUnity;

public class ImgShowCanvas : MonoBehaviour
{

	private GameObject canvas;

	private int rows;
	private int cols;
	private int numImages = 0;

	private float width;
	private float height;
	private float colStep;
	private float rowStep;


	private bool hasShapesBeenGenerated;
	private bool hasFinalBeenAdded;
	private bool hasFinalBeenCreated;

	private Mat shapes;
	private Mat final;
	private Mat original;

	private List<GameObject> m_canvasImages;
	private List<GameObject> m_canvasTitles;

	private List<Mat> m_MatList;
	private List<string> m_TitleList;

	private int MAX_ROW_ELEMENTS = 2;

	private static ImgShowCanvas instance;

	void Awake(){
		instance = this;
	}

	void Start(){
		canvas = SceneObjects.getInstance ().CanvasImages;
		canvas.GetComponent<RectTransform> ().sizeDelta = 
			new Vector2 (canvas.transform.parent.GetComponent<RectTransform>().rect.width, 
				canvas.transform.parent.GetComponent<RectTransform>().rect.height);


		numImages = 0;
		m_MatList = new List<Mat>();
		m_TitleList = new List<string>();
		m_canvasImages = new List<GameObject> ();
		m_canvasTitles = new List<GameObject> ();

		shapes = MatContainer.getInstance ().M_8UC3;
		final = MatContainer.getInstance ().M_8UC3;

		hasShapesBeenGenerated = false;
		hasFinalBeenAdded = false;
		hasFinalBeenCreated = false;
	}

	public static ImgShowCanvas getInstance(){
		return instance;
	}

	public void checkFinalCreated(){
		if (!hasFinalBeenCreated) {
			MatContainer.getInstance().originalMat.copyTo (final);
			hasFinalBeenCreated = true;
		}
	}

	public void addMatAndTitle(Mat m, string t){

		m_MatList.Add (m);
		m_TitleList.Add (t);
		numImages++;
	}

	public void resetMatList(){
		m_TitleList.Clear ();
		m_MatList.Clear ();
		numImages = 0;

		if (hasShapesBeenGenerated) {
			hasShapesBeenGenerated = false;
			shapes.setTo (new Scalar (0));
		}

		hasFinalBeenAdded = false;
		hasFinalBeenCreated = false;
	}

	public void ClearCanvas(){
		for (int i = 0; i < m_canvasImages.Count; i++) {
		
			Destroy (m_canvasImages [i]);
			if (m_canvasTitles [i] != null)
				Destroy (m_canvasTitles[i]);
		}

		m_canvasImages.Clear ();
	
	}
		

	public void imshow( bool isCardboard, int NewRows = 0, int NewCols = 0){

		if (isCardboard) {
			if (hasShapesBeenGenerated) {
				Texture2D texture = new Texture2D (final.cols (), final.rows (), TextureFormat.RGBA32, false);
				Utils.matToTexture2D (final, texture);
				this.gameObject.GetComponent<Renderer> ().material.mainTexture = texture;
			} else {
				this.gameObject.GetComponent<Renderer> ().material.mainTexture = MatContainer.getInstance().wcTexture;

			}




		} else {
			if (!hasFinalBeenAdded) {
				prepareToPrintFinal ();
				hasFinalBeenAdded = true;
			}
				

			if (m_canvasImages.Count != numImages) { //If the num of canvas is different to the num of Mats to show
				numImages = m_MatList.Count;
				setNumElements (numImages, NewRows, NewCols);
				//cleanCanvas ();
				buildCanvas ();
			}
				
			for (int i = 0; i < numImages; i++) {
				//Convertim Mat a Texture2D per poder mostrar-la per pantalla
				Mat m = m_MatList [i];
				Texture2D texture = new Texture2D (m.cols (), m.rows (), TextureFormat.RGBA32, false);
				Utils.matToTexture2D (m, texture);

				m_canvasImages [i].GetComponent<RawImage> ().texture = texture;

				if (m_TitleList != null && m_TitleList [i] != null) {
					m_canvasTitles [i].GetComponent<Text> ().text = m_TitleList [i];
				}
			}
		}

	}

	private void buildCardboardCanvas(){
		canvas.GetComponent<CanvasScaler> ().uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
		canvas.GetComponent<CanvasScaler> ().screenMatchMode = CanvasScaler.ScreenMatchMode.Expand;
		width = canvas.transform.parent.GetComponent<RectTransform> ().rect.width;
		height = canvas.transform.parent.GetComponent<RectTransform> ().rect.height;

		m_canvasImages.Add (createCarboardImage (0));
		m_canvasImages.Add (createCarboardImage (1));

	}


	private GameObject createCarboardImage(int index){
		float imgWidthPosition;
		float imgHeightPosition = 0;

		if (index == 0) {
			imgWidthPosition = -width/4;
		} else {
			imgWidthPosition = width/4;
		}

		GameObject img = new GameObject ();
		img.AddComponent<RawImage> ();
		img.transform.parent = canvas.transform;

		img.GetComponent<RectTransform>().anchoredPosition = new Vector2(imgWidthPosition,imgHeightPosition );
		img.transform.localScale =  new Vector3 (3.0f, 3.0f, 3.0f);
		//img.GetComponent<RectTransform> ().rect.height = height;
		//img.GetComponent<RectTransform> ().rect.width = width / 2;

		return img;
	}

	public void cleanCanvas(){
		for (int i = 0; i < canvas.transform.childCount; i++) {
			canvas.transform.GetChild (i).GetComponent<Renderer> ().enabled = false;
			Destroy (canvas.transform.GetChild (i).gameObject);
		}
	}

	private void setNumElements(int numel, int newRows, int newCols){
		numImages = numel;

		if (newRows == 0 || newCols == 0) {
			if (numel > MAX_ROW_ELEMENTS) {
				cols = MAX_ROW_ELEMENTS;
				rows = numImages/MAX_ROW_ELEMENTS;
				if (numImages % MAX_ROW_ELEMENTS != 0)
					rows++;
			} else {
				cols = numel;
				rows = 1;
			}
		}

		width = canvas.transform.parent.GetComponent<RectTransform> ().rect.width;
		height = canvas.transform.parent.GetComponent<RectTransform> ().rect.height;
		colStep = (width) / cols;
		rowStep = height / rows;
	}

	private void buildCanvas(){

		m_canvasImages.Clear ();
		m_canvasTitles.Clear ();

		int r = 0;
		int c = 0;

		for (int count = 0; count < numImages; count++) {

			GameObject img = new GameObject ();
			img.AddComponent<RawImage> ();
			img.transform.SetParent( canvas.transform);

			float imgWidthPosition = -width / 2 + c * colStep + colStep / 2;
			float imgHeightPosition = height / 2 - r * rowStep - rowStep / 2;
			img.GetComponent<RectTransform>().anchoredPosition = new Vector2(imgWidthPosition,imgHeightPosition );

			float imgHeight = img.GetComponent<RectTransform> ().rect.height/2.0f;
			float imgWidth = img.GetComponent<RectTransform> ().rect.width/2.0f;
			GameObject txt = Instantiate (Resources.Load ("Prefabs/UIText"), new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
			txt.transform.SetParent( canvas.transform);
			txt.GetComponent<RectTransform>().anchoredPosition = new Vector2(imgWidthPosition + imgWidth,imgHeightPosition + imgHeight);
			txt.GetComponent<Text> ().text = "Img: " + count;

			m_canvasImages.Add (img);
			m_canvasImages [count].transform.localScale = new Vector3 (2.0f, 2.0f, 2.0f);
			m_canvasTitles.Add (txt);

			c++;
			if (c == cols) {
				r++;
				c = 0;
			}
		}

		//m_canvasImages [numImages - 1].GetComponent<RectTransform> ().anchoredPosition = new Vector2 (width / 2, 0);
		//m_canvasImages[numImages-1].transform.localScale = new Vector3 (3.0f, 3.0f, 3.0f); 



		if (numImages == 1)
			m_canvasImages [0].transform.localScale = new Vector3 (3.0f, 3.0f, 3.0f);
	}
		

	public void generateCircleShapesMat(Mat circles, bool printShapes){
		if (printShapes) {
			bool mustAdd = false;
			if (!hasShapesBeenGenerated) {
				hasShapesBeenGenerated = true;
				mustAdd = true;
			}

			shapes = drawCircles (shapes, circles);

			if (mustAdd)
				addMatAndTitle (shapes, "shapes");//shapes is black background with drawn shapes.
		}

		checkFinalCreated ();
		final = drawCircles (final, circles);


	}

	public void prepareToPrintFinal(){
		addMatAndTitle (final, "final");
	}

	public void generatePolyShapesMat(List<MatOfPoint> polys, bool printShapes, int edges = -1){

		if (printShapes) {
			bool mustAdd = false;
			if (!hasShapesBeenGenerated) {
				hasShapesBeenGenerated = true;
				mustAdd = true;
			}
				
			shapes = drawPolys (shapes, polys, edges);
			if (mustAdd)
				addMatAndTitle (shapes, "shapes");	
		
		}

		checkFinalCreated ();
		final = drawPolys (final, polys, edges);


	}
		

	private Mat drawPolys( Mat outMat, List<MatOfPoint> polys, int edges){
		for (int i = 0; i < polys.Count; i++) {


			if ( Imgproc.contourArea (polys[i]) > 250 && !ImageOperations.getInstance().isPolyInsideCircle(polys[i]) ) {

				if (!checkEdges ((int)polys [i].total(), edges))
					continue;

				Scalar col = null;
				if (polys[i].total () == 3) {
					//triangles.Add (aux);
					col = new Scalar (255, 0, 0, 255);
				} else if (polys[i].total () == 4) {
					//rectangles.Add (aux);
					col = new Scalar (0, 255, 0, 255);
				} else {
					//polygons.Add (aux);
					col = new Scalar (0, 0, 255, 255);
				}


				Core.fillPoly (outMat, new List<MatOfPoint>(new MatOfPoint[]{polys[i]}), col, 8, 0, new Point (0.0, 0.0));

			}
		}

		return outMat;
	}

	private bool checkEdges(int edges, int reqEdges){
		if (reqEdges == -1)
			return true;
		return edges == reqEdges;
		
	}


	private Mat drawCircles(Mat outMat, Mat circles){
		Scalar col = new Scalar (125, 0, 125, 255);

		for (int x = 0; x < circles.cols(); x++) 
		{
			double []vCircle = circles.get(0,x);

			if (vCircle == null) {
				continue;
			}

			//vCircle[0] i [1] --> center point. vCircle[3] --> radius
			Point pt = new Point((int)Mathf.RoundToInt((float)vCircle[0]), (int)Mathf.RoundToInt((float)vCircle[1]));
			int radius = (int)Mathf.RoundToInt((float)vCircle[2]);


				// draw the found circle
				//circle (Mat img, Point center, int radius, Scalar color, int thickness, int lineType, int shift)
			Core.circle(outMat, pt, radius, col, -1, 8, 0);

		}

		return outMat;
	}
		

}

