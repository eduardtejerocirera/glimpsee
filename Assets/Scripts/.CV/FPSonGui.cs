﻿using UnityEngine;
using System.Collections;

public class FPSonGui : MonoBehaviour {


	#region public fields

	public float m_frequency = 0.0F; // The update frequency of the fps
	public int nbDecimal = 1; // decimals to display

	#endregion

	#region private fields

	private float m_timeAccumulated = 0f; // FPS accumulated over the interval
	private int m_frames = 0; // Frames drawn over the interval
	private string m_sFPS = ""; // The fps formatted into a string.
	private Color m_color = Color.red; // The color of the GUI, depending of the FPS ( R < 10, Y < 30, G >= 30 )
	private GUIStyle style; // The style the text will be displayed at, based en defaultSkin.label.

	#endregion

	#region public methods

	void Awake () {
		// 0 for no sync, 1 for panel refresh rate, 2 for 1/2 panel rate
		//For a 60Hz screen 1=60fps, 2=30fps, 0=don't wait for sync.
		//QualitySettings.vSyncCount = 1;
	}

	// Use this for initialization
	void Start () {
		
		StartCoroutine(CountFPS());
	}
	
	// Update is called once per frame
	void Update () {
		
		
		m_timeAccumulated += Time.timeScale / Time.deltaTime;
		++m_frames;
	}
	
	
	IEnumerator CountFPS()
	{
		// this infinite loop is executated every (frequency) thanks to the corroutine
		while (true)
		{
			// Update the FPS
			float fps = m_timeAccumulated / m_frames;
			m_sFPS = fps.ToString("f" + Mathf.Clamp(nbDecimal, 0, 0));
			
			//Update the color
			m_color = (fps >= 30) ? Color.green : Color.red;

			m_timeAccumulated = 0.0F;
			m_frames = 0;
			
			yield return new WaitForSeconds(m_frequency);
		}
	}
	
	void OnGUI()
	{
		int w = Screen.width, h = Screen.height;
		Rect rect = new Rect(10, h * 7 / 8, w, h * 8 / 100);
		
		// Copy the default label skin, change the color and the alignement
		if (style == null)
		{
			style = new GUIStyle(GUI.skin.label);
			style.normal.textColor = Color.white;
			style.alignment = TextAnchor.UpperLeft;
			style.fontSize = h * 4 / 100;
		}
		
		style.alignment = TextAnchor.UpperLeft;
		style.fontSize = h * 4 / 100;
		
		GUI.color = m_color;
		GUI.Label(rect, " fps: " + m_sFPS , style);
	}


	#endregion


	#region private methods


	#endregion
	
}


	
	


