﻿using UnityEngine;
using System.Collections;
using OpenCVForUnity;

public class InitialSettings : MonoBehaviour
{
	public bool isCardboard;
	public bool useWebCam;
	public bool showAll;
	public bool sizeSetCorrectly;

	private WebCamTexture webCamTexture;
	private WebCamDevice webCamDevice;
	private Color32[] colors;

	private int width;
	private int height;

	private GameObject imageCanvas;
	private GameObject cardboard;

	void Start () {

		imageCanvas = SceneObjects.getInstance ().CanvasImages;
		cardboard = SceneObjects.getInstance ().CardBoardMain;

		if (isCardboard) {
			useWebCam = true;
			showAll = false;
			cardboard.SetActive (true);
			imageCanvas.SetActive (false);
		}
		else{
			cardboard.SetActive (false);
			imageCanvas.SetActive (true);
		}	



		sizeSetCorrectly = false;

		if (!useWebCam) {
			loadMatImage (); //load from resources
		} else {
			loadWebCamMat (); // prepare webcam
		}

		StartCoroutine ("checkWebcam");
	}





	private Mat loadMatImage(){

		Texture2D imgTexture = Resources.Load ("Img/shapes") as Texture2D;
		/*CV_XYZN
		 * X --> bits per item
		 * Y --> signed (S) or unsigned (U)
		 * Z --> variable type prefix. Char (C)
		 * N --> channels per matrix point. BGR: 3 channels
		 * */

		MatContainer.getInstance ().setDimensions (imgTexture.height, imgTexture.width, useWebCam);
		Mat outMat = MatContainer.getInstance ().M_8UC3;

		Utils.texture2DToMat (imgTexture, outMat);
		return outMat;
	}


	private void loadWebCamMat(){

		if (webCamTexture == null) {
			webCamDevice = WebCamTexture.devices [0];
			webCamTexture = new WebCamTexture (webCamDevice.name, 640, 480);
		}
		webCamTexture.Play ();
	}

	IEnumerator checkWebcam(){
		while (!sizeSetCorrectly) {
			checkWebCamTextureSize ();
			yield return 0; 
		}
	}

	/*private Mat loadWebCamMat(Mat outMat = null){

		if (webCamTexture == null) {
			webCamDevice = WebCamTexture.devices [0];
			webCamTexture = new WebCamTexture (webCamDevice.name, 640, 480);
		}
		webCamTexture.Play ();
	
		return retrieveInitial (outMat);
	}*/


	private Mat outMat = null;
	public Mat retrieveInitial(){

		if(outMat == null)
			outMat = MatContainer.getInstance().M_8UC4;

		Utils.webCamTextureToMat (webCamTexture, outMat, MatContainer.getInstance ().colors);

		MatContainer.getInstance ().wcTexture = webCamTexture;

		return outMat;
	}

	private void checkWebCamTextureSize(){
		//Size ONLY becomes correct after some time (0.5 secs). Before that, it's a number < 100
		if (webCamTexture.width > 100){
			sizeSetCorrectly = true;
			MatContainer.getInstance().setDimensions(webCamTexture.width, webCamTexture.height, useWebCam);
			this.gameObject.GetComponent<Looper> ().enabled = true;
			this.gameObject.GetComponent<ImgShowCanvas> ().enabled = true;
			SceneObjects.getInstance ().GameLogic.SetActive (true);

			SceneObjects.getInstance ().ArQuad.SetActive (true);
			SceneObjects.getInstance ().OutQuad.SetActive (true);
		}
	}

	/*void Update(){
		if (!sizeSetCorrectly) {
			checkWebCamTextureSize ();
		}
	}*/



	public WebCamTexture getTexture(){
		return webCamTexture;
	}
		


}

