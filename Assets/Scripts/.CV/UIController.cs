﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIController : MonoBehaviour {

	private GameObject goToMenuButton;
	private GameObject programSelectionView;

	private float width;
	private float height;

	private Looper m_looper;

	private List<string> buttonNames;


	void Start(){

		goToMenuButton = this.transform.Find ("MenuButton").gameObject;

		programSelectionView = this.transform.Find ("ProgramSelection").gameObject;

		width = GetComponent<RectTransform> ().rect.width;
		height = GetComponent<RectTransform> ().rect.height;

		buildButtons ();
		programSelectionView.SetActive (false);

		m_looper = SceneObjects.getInstance ().Main.GetComponent<Looper> ();

		GetComponent<Image> ().enabled = false;




		setSize (programSelectionView, width, height);

	}

	private void buildButtons(){

		int totalPrograms = Looper.programs.Length;
		int upMargin = 50, downMargin = 10;
		float step = (height - upMargin - downMargin) / totalPrograms;

		for (int i = 0; i < totalPrograms; i++) {
			GameObject btn = (GameObject)Instantiate(Resources.Load("Prefabs/ProgramButton"));
			btn.transform.SetParent (programSelectionView.transform); 
			//btn.transform.position = new Vector3 (0, height / 2 - upMargin - i * step, btn.transform.position.z);

			btn.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, +height / 2 - upMargin - i * step);
			//btn.GetComponent<RectTransform> ().position.x = 0;

			btn.name = Looper.programs[i];
			btn.transform.GetChild(0).GetComponent<Text>().text = Looper.programs[i];
			btn.GetComponent<Button>().onClick.AddListener(delegate { ProgramSelected(btn.name); });

		}
	}

	private void setSize(GameObject g, float width, float height){
		g.GetComponent<RectTransform> ().sizeDelta = new Vector2 (width, height); 
	}

	#region listeners

	public void goToMenu(){
		goToMenuButton.SetActive (false);
		programSelectionView.SetActive (true);
		GetComponent<Image> ().enabled = true;

		SceneObjects.getInstance ().CanvasImages.SetActive (false);
	} 
		
	public void ProgramSelected(string btnName){

		m_looper.loadProgram (btnName);
	
		goToMenuButton.SetActive (true);
		programSelectionView.SetActive (false);
		GetComponent<Image> ().enabled = false;

		SceneObjects.getInstance ().CanvasImages.SetActive (true);
	}
	#endregion
}
