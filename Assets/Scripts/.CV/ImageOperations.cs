﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using OpenCVForUnity;
public enum TypeOfDetection {Poly, Circle, PolyCircle};

public class ImageOperations : MonoBehaviour {


	//public bool useWebCam;
	//public bool showAll;
	//public TypeOfDetection detecType;
	private int caseIndex;


	//public ImgShowCanvas imgShowCanvas;

	/*private List<Mat> matList;
	private List <string> titles;

	private Mat loadedMat;
	private Mat currentMat;
	private Mat finalResult;*/

	private WebCamTexture webCamTexture;
	private WebCamDevice webCamDevice;
	private Color32[] colors;

	private static ImageOperations instance;


	#region public methods

	public static ImageOperations getInstance(){
		return instance;
	}

	public Mat GRAYSCALE(Mat m){
		//m = getInitialMat (useWebCam);

		//PyrSmoothing ();
		m = GaussianSmoothing (m);
		m = ConvertToGrayscale (m);
		m = Canny (m);
		return m;
	}

	public Mat HOUGH(Mat m){
		return HoughCircles(m);
	}

	public List<MatOfPoint> FINDCONTOURS(Mat m){
		List<MatOfPoint> l = FindContours (m);
		return l;
	}


	/*public List<MatOfPoint> POLY_COLOR(Mat m){
		List<MatOfPoint> l = FindContours (m);
		return l;
	}*/

	public void RESETMAT(){
		MatContainer.getInstance ().resetAll ();
	}

	public Mat INRANGE(Mat m, string s){
		m = GaussianSmoothing (m);
		m = inRange (m, s);
		m = morphOps (m);
		return m;
	
	}

	/*public void SHOWIMAGES(){
		//drawShapesToMats (new List<Mat>{MatContainer.getInstance().shapesMat, MatContainer.getInstance().finalMat});
		showImages ();
	}*/


	#endregion


	//Init methods are only called once, at the beggining of execution
	#region init
	void Awake(){
		instance = this;
	}

	/*void Start () {
		if (!useWebCam) {
			loadMat (); //load from resources
		} else {
			initWebCam (); // prepare webcam
		}

		//caseIndex = getCaseIndex ();
	}*/

	/*private int getCaseIndex(){
		if (detecType == TypeOfDetection.Poly)
			return 0;
		if (detecType == TypeOfDetection.Circle)
			return 1;
		if (detecType == TypeOfDetection.PolyCircle)
			return 2;

		return -1;
	}*/
		
	/*private void loadMat(){

		Texture2D imgTexture = Resources.Load ("Img/shapes") as Texture2D;
		//CV_XYZN
		 //* X --> bits per item
		 //* Y --> signed (S) or unsigned (U)
		 //* Z --> variable type prefix. Char (C)
		 //* N --> channels per matrix point. BGR: 3 channels
		 //*

		MatContainer.getInstance ().setDimensions (imgTexture.height, imgTexture.width, useWebCam);

		Utils.texture2DToMat (imgTexture, MatContainer.getInstance().loadedMat);
		//Debug.Log ("w:" +imgTexture.width +  ",h:" + imgTexture.height);
		//Debug.Log ("imgMat dst ToString " + loadedMat.ToString ());

	}

	private void initWebCam(){

		if (webCamTexture == null) {
			webCamDevice = WebCamTexture.devices [0];
			webCamTexture = new WebCamTexture (webCamDevice.name, 640, 480);
		}


		//Debug.Log ("width " + webCamTexture.width + " height " + webCamTexture.height + " fps " + webCamTexture.requestedFPS);

		//if colors only initialised here, UNITY CRASHES!!!
		//colors = new Color32[webCamTexture.width * webCamTexture.height];
		webCamTexture.Play ();
	}*/

	#endregion



	
	// Update is called once per frame
	/*void Update () {
		getInitialMat ();

		//PyrSmoothing ();
		GaussianSmoothing (MatContainer.getInstance().gaussianMat, MatContainer.getInstance().currentMat);
		ConvertToGrayscale (MatContainer.getInstance().grayscaleMat, MatContainer.getInstance().currentMat);

		switch (caseIndex) {
		case 0:
			//POLY
			Canny (MatContainer.getInstance().cannyMat, MatContainer.getInstance().currentMat);
			FindContours (MatContainer.getInstance().shapesMat, MatContainer.getInstance().currentMat);
			break;
		case 1: 
			//CIRCLE
			HoughCircles(MatContainer.getInstance().shapesMat, MatContainer.getInstance().currentMat);
			break;
		case 2: 
			//BOTH
			HoughCircles (MatContainer.getInstance().shapesMat, MatContainer.getInstance().currentMat);
			Canny (MatContainer.getInstance().cannyMat, MatContainer.getInstance().currentMat);
			FindContours (MatContainer.getInstance().shapesMat, MatContainer.getInstance().currentMat);
			break;
		default:
			Debug.Log ("I shouldn't be here");
			break;
		}

		drawShapesToMats (new List<Mat>{MatContainer.getInstance().shapesMat, MatContainer.getInstance().finalMat});
		showImages ();
		MatContainer.getInstance ().resetAll ();

		if (!useWebCam)
			this.gameObject.SetActive (false); // stop update loop

	
	}*/



	/*private void getInitialMat(){
		if (useWebCam){
			MatContainer.getInstance().setDimensions(webCamTexture.width, webCamTexture.height, useWebCam);
			//colors = new Color32[MatContainer.getInstance ().total_px];

			Utils.webCamTextureToMat (webCamTexture, MatContainer.getInstance().loadedMat, MatContainer.getInstance().colors);
		}

		MatContainer.getInstance().addMatToShow(MatContainer.getInstance().loadedMat, "original"); 
		MatContainer.getInstance ().updateCurrent (MatContainer.getInstance ().loadedMat);
	}*/

	/*private Mat getInitialMat(bool useWebCam){
		Mat outMat;

		if (useWebCam) {
			//MatContainer.getInstance().setDimensions(webCamTexture.width, webCamTexture.height, useWebCam);
			//colors = new Color32[MatContainer.getInstance ().total_px];
			outMat = MatContainer.getInstance().M_8UC4;
			Utils.webCamTextureToMat (webCamTexture, outMat, MatContainer.getInstance ().colors);
		} else {
			outMat = MatContainer.getInstance ().M_8UC3;
		}

		return outMat;
	}*/



	#region imgProc

	/*private void PyrSmoothing(){
		Mat pyrMat = new Mat (currentMat.cols()/2, currentMat.rows()/2, CvType.CV_8UC3);

		Imgproc.pyrDown( currentMat, pyrMat, new Size( currentMat.cols()/2, currentMat.rows()/2 ) );
		Imgproc.pyrUp (pyrMat, pyrMat, new Size (pyrMat.cols() * 2.0f, pyrMat.rows() * 2.0f));

		updateStruct (pyrMat, "Pyr");
	}*/


	private Mat GaussianSmoothing(Mat inMat){
		Mat outMat = MatContainer.getInstance ().M_8UC3;
		Imgproc.GaussianBlur (inMat, outMat, new Size(3,3), 0.0f,0.0f);
		return outMat;
	}


	/*private void GaussianSmoothing(Mat m, Mat currentMat){
		Imgproc.GaussianBlur (currentMat, m, new Size(3,3), 0.0f,0.0f);

		MatContainer.getInstance().addMatToShow (m, "Gaussian Smooth");
		MatContainer.getInstance ().updateCurrent (m);
	}*/

	private Mat ConvertToGrayscale(Mat inMat){
		Mat outMat = MatContainer.getInstance ().M_8UC1;
		Imgproc.cvtColor (inMat, outMat, Imgproc.COLOR_RGB2GRAY);
		return outMat;
	}

	/*private void ConvertToGrayscale(Mat m, Mat currentMat){

		Imgproc.cvtColor (currentMat, m, Imgproc.COLOR_RGB2GRAY);

		MatContainer.getInstance().addMatToShow (m, "grayscale");
		MatContainer.getInstance ().updateCurrent (m);
	}*/

	private Mat Canny(Mat inMat){
		Mat outMat = MatContainer.getInstance ().M_8UC1;
		Imgproc.Canny (inMat, outMat, 50, 200);
		return outMat;
	}

	/*private void Canny(Mat m, Mat currentMat){
		Imgproc.Canny (currentMat, m, 50, 200);

		MatContainer.getInstance().addMatToShow (m, "canny");
		MatContainer.getInstance ().updateCurrent (m);
	}*/


	private List<MatOfPoint> FindContours(Mat inMat){
		List<MatOfPoint> c = new List<MatOfPoint> ();
		Mat h = new Mat ();
		Imgproc.findContours (inMat, c, h, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

		List<MatOfPoint> outList = new List<MatOfPoint> ();
		for (int i = 0; i < c.Count; i++) {
			outList.Add (AproximatePoly (c [i]));
		}

		return outList;
	}

	/*private void FindContours(Mat shapes, Mat currentMat){
		List<MatOfPoint> c = new List<MatOfPoint> ();
		Mat h = new Mat ();
		Imgproc.findContours (currentMat, c, h, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

		for (int i = 0; i < c.Count; i++) {
			MatContainer.getInstance ().polys.Add (AproximatePoly (c [i]));
		}
	}*/

private MatOfPoint AproximatePoly(MatOfPoint contour){
		MatOfPoint2f mopf = new MatOfPoint2f ();//Stores the contour points in MatOfPoint2f
		MatOfPoint2f o = new MatOfPoint2f ();//Stores the aproxPoly result in MatOfPoint2f
		contour.convertTo(mopf, CvType.CV_32FC2);
		Imgproc.approxPolyDP(mopf, o, 10.0f, true);

		MatOfPoint aux = new MatOfPoint ();//Stores the aproxPoly result in MatOfPoint
		o.convertTo(aux, CvType.CV_32S);

		return aux; 
	}


	/*private void HoughCircles(Mat shapes, Mat currentMat){
		// circles will store circle radius, center, ...

		Imgproc.HoughCircles(
			currentMat, //origin
			MatContainer.getInstance().circles, //dest
			Imgproc.CV_HOUGH_GRADIENT, //method
			1.8, //dp
			100,//mindist
			200,//param1-> high Canny Threshold
			90,//param2 -> accum threshold
			6,//minRadius
			100);//maxRadius
	}*/

	private Mat HoughCircles(Mat inMat){
		// circles will store circle radius, center, ...

		Mat outMat = MatContainer.getInstance ().M_8UC3;

		Imgproc.HoughCircles(
			inMat, //origin
			outMat, //dest
			Imgproc.CV_HOUGH_GRADIENT, //method
			1.8, //dp
			100,//mindist
			200,//param1-> high Canny Threshold
			90,//param2 -> accum threshold
			6,//minRadius
			100);//maxRadius
		return outMat;
	}

	public Mat inRange(Mat m, string s){
		Mat outMat =  MatContainer.getInstance ().M_8UC1;
		Mat hsvMat = MatContainer.getInstance ().M_8UC3;
		//Core.inRange (m, new Scalar(0,0,0), new Scalar(255,255,255), outMat);
		//return outMat;


		ColorObject col = new ColorObject (s); //s can be "blue", "red", "yellow", "green"

		//first find blue objects
		Imgproc.cvtColor (m, hsvMat, Imgproc.COLOR_RGB2HSV);
		Core.inRange (hsvMat, col.getHSVmin (), col.getHSVmax (), outMat);
		return outMat;
	}

	private Mat morphOps (Mat m)
	{
		
		Mat outMat = MatContainer.getInstance ().M_8UC1;

		//create structuring element that will be used to "dilate" and "erode" image.
		//the element chosen here is a 3px by 3px rectangle
		Mat erodeElement = Imgproc.getStructuringElement (Imgproc.MORPH_RECT, new Size (3, 3));
		//dilate with larger element so make sure object is nicely visible
		Mat dilateElement = Imgproc.getStructuringElement (Imgproc.MORPH_RECT, new Size (3, 3));

		Imgproc.erode (m, outMat, erodeElement);
		//Imgproc.erode (outMat, outMat, erodeElement);

		//Imgproc.dilate (outMat, outMat, dilateElement);
		//Imgproc.dilate (outMat, outMat, dilateElement);

		return outMat;
	}

	//private Mat inRangeHSV(){

	//}
		
	#endregion

	#region MatManagement
	/*private void updateStruct(Mat x, string t){
		if (matList == null) matList = new List<Mat> ();
		if (titles == null) titles = new List<string> ();

		matList.Add (x);
		titles.Add (t);
		currentMat = x;
	}*/

	/*private void resetStruct(){
		matList = null;
		titles = null;
		currentMat = null;
		MatContainer.getInstance ().resetAll ();
	}*/
	#endregion


	#region canvas
	/*private void showImages(){

		//imgShowCanvas.cleanCanvas (); // AIXO PETA!!!!!

		if (showAll) {
			imgShowCanvas.imshow (MatContainer.getInstance().matList, MatContainer.getInstance().titles);
		} else {
		imgShowCanvas.imshow (new List<Mat> (new Mat[]{MatContainer.getInstance().finalMat}), 
				new List<string>(new string[]{""}));
		}
	}*/

	/*private void drawShapesToMats(List<Mat> mats){
		MatContainer.getInstance ().prepareTodraw ();
		
		switch (caseIndex) {
			case 0://POLY
				drawPolysToMats(MatContainer.getInstance().polys, mats);
				break;
			case 1://CIRCLE
				drawCirclesToMats(MatContainer.getInstance().circles, mats);
				break;
			case 2://BOTH
				drawPolysToMats (MatContainer.getInstance().polys, mats);
				drawCirclesToMats (MatContainer.getInstance().circles, mats);
				break;
			default:
				Debug.Log ("I shouldn't be here");
				break;
		}
		for (int k = 0; k < mats.Count;k++){
			MatContainer.getInstance ().addMatToShow (mats [k], k.ToString ());
		}
		
	}*/

	/*private void drawPolysToMats(List<MatOfPoint> polys, List<Mat> mats){
		for (int i = 0; i < polys.Count; i++) {
			

		if ( Imgproc.contourArea (polys[i]) > 250 && !isPolyInsideCircle(polys[i]) ) {

				Scalar col = null;
				if (polys[i].total () == 3) {
					//triangles.Add (aux);
					col = new Scalar (255, 0, 0, 255);
				} else if (polys[i].total () == 4) {
					//rectangles.Add (aux);
					col = new Scalar (0, 255, 0, 255);
				} else {
					//polygons.Add (aux);
					col = new Scalar (0, 0, 255, 255);
				}

				for (int k = 0; k < mats.Count; k++) {
					Core.fillPoly (mats[k], new List<MatOfPoint>(new MatOfPoint[]{polys[i]}), col, 8, 0, new Point (0.0, 0.0));
				}
			}
		}
	}*/

	/*private void drawCirclesToMats(Mat circles, List<Mat> mats){
		Scalar col = new Scalar (125, 0, 125, 255);

		for (int x = 0; x < circles.cols(); x++) 
		{
			double []vCircle = circles.get(0,x);

			if (vCircle == null) {
				continue;
			}

			//vCircle[0] i [1] --> center point. vCircle[3] --> radius
			Point pt = new Point((int)Mathf.RoundToInt((float)vCircle[0]), (int)Mathf.RoundToInt((float)vCircle[1]));
			int radius = (int)Mathf.RoundToInt((float)vCircle[2]);


			for (int k = 0; k < mats.Count; k++) {
				// draw the found circle
				//circle (Mat img, Point center, int radius, Scalar color, int thickness, int lineType, int shift)
				Core.circle(mats[k], pt, radius, col, -1, 8, 0);
			}

			//Core.circle(shapes, pt, radius, col, -1, 8, 0);
			//Core.circle(MatContainer.getInstance().finalMat, pt, radius, col, -1, 8, 0);
		}
	}*/

	public bool isPolyInsideCircle(MatOfPoint poly){
	//poly es una matriu dels punts del poligon (3 si es triangle, 4 si quadrat,...)
		Point[] polyArray = new Point[poly.total()];
		polyArray = poly.toArray ();

		for (int j = 0; j < polyArray.Length; j++) {
			for (int k = 0; k < MatContainer.getInstance().Circles.cols(); k++){
				if ( isPointInsideCircle (polyArray[j], MatContainer.getInstance().Circles.get(0,k)) )
					return true;
				}
		}
		return false;
		
	}

	private bool isPointInsideCircle(Point point, double[] circle){
	float d = Mathf.Pow ((float)point.x - (float)circle [0], 2) + Mathf.Pow ((float)point.y - (float)circle [1], 2);
		d = Mathf.Sqrt (d);
		
		if (d < circle [2])
			return true;

		return false;
	}
	#endregion


}

