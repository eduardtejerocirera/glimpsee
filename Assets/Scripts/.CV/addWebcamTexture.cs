﻿using UnityEngine;
using System.Collections;
using OpenCVForUnity;

public class addWebcamTexture : MonoBehaviour {

	// Use this for initializa
	void Start () {

		WebCamTexture wt = MatContainer.getInstance ().wcTexture;
		this.gameObject.GetComponent<Renderer> ().material.mainTexture = wt;
		this.gameObject.transform.localScale = new Vector3 (wt.width, wt.height, 1);
	}

}
