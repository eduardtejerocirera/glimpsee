﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OpenCVMarkerBasedAR;
using OpenCVForUnity;

public class MarkerTictactoe : MonoBehaviour
{
	public const byte CASELLA_0 = 75;
	public const byte CASELLA_1 = 95;
	public const byte CASELLA_2 = 91;
	public const byte CASELLA_3 = 94;
	public const byte CASELLA_4 = 74;
	public const byte CASELLA_5 = 78;
	public const byte CASELLA_6 = 65;
	public const byte CASELLA_7 = 85;
	public const byte CASELLA_8 = 81;

	public const byte FITXA = 64;

	private byte[] emptyCaselles = {
		CASELLA_0,
		CASELLA_1,
		CASELLA_2,
		CASELLA_3,
		CASELLA_4,
		CASELLA_5,
		CASELLA_6,
		CASELLA_7,
		CASELLA_8
	};

	public const int STATE_CHANGE = 3;
	public const int MAX_ERRORS = 30;


	private byte[] currentCaselles = new byte[9];
	private byte[] tempCaselles = new byte[9];

	//bool[] foundCaselles = new bool[9]{ false, false, false, false, false, false, false, false, false };
	private MarkerTictactoeController mtController;


	private MarkerDetection mDetection;
	public int changeStateCounter = 0;
	public int errorCounter = 0;
	// Use this for initialization
	void Start ()
	{
		//mDetection = SceneObjects.getInstance ().MarkerDetecScript;
		mDetection = SceneObjects.getInstance ().MarkerDetecScript;

		mtController = SceneObjects.getInstance ().mtController;
	}

	public void analiseMarkers(Mat m){
		int num = mDetection.howManyDetected (m);
		if ( num == 9) {
			lookForChanges (mDetection.markerDetector.getFindMarkers ());
		} else if (changeStateCounter > 0){
			errorCounter++;
			if (errorCounter == MAX_ERRORS) {
				changeStateCounter = 0; //RESET, too many errors
				errorCounter = 0;
			}

		}
			
	}

	private byte[] aux = new byte[9];
	private void lookForChanges(List<Marker> foundMarkers ){
	
		for(int i = 0; i < foundMarkers.Count; i++){
			aux [i] = (byte)foundMarkers [i].id;
		}

		fillOrderedIds (aux);

		if (changeStateCounter == 0 && !areEqual (currentCaselles, aux)) { //comparem amb les caselles actuals reals
			errorCounter = 0;
			changeStateCounter++; // primera vegada que entrem, actualitzem tempcaselles amb els 9 ids detectats.
			//Debug.Log ("Increment First");
			copy (aux, tempCaselles);
		} else if (changeStateCounter > 0 && areEqual (tempCaselles, aux)) {
			changeStateCounter++; // tornem a entrar, tempCaselles i aux han de ser iguals.
			if (changeStateCounter == STATE_CHANGE) {
				updateCurrentCaselles();// a les 3 coincidencies considerem que hi ha hagut un canvi i actualitzem.
				changeStateCounter = 0;
			}
		} else if(changeStateCounter > 0 && !areEqual(tempCaselles, aux)){
			errorCounter++;// no hem arribat a les 3 coincidencies i son diferents.
			if (errorCounter == MAX_ERRORS) {
				errorCounter = 0;
				changeStateCounter = 0;
			}
			//Debug.Log("Reset, are different");
		}
	}

	private void updateCurrentCaselles(){
		if (areEqual (tempCaselles, emptyCaselles)) {
			Debug.Log ("START!!!!!!!!!");
			mtController.goStartPartida ();
		} else {
			Debug.Log ("NEW FITXA!!!!!!!!!!");
			mtController.novaFitxa (getFitxaId());
		}

		copy (tempCaselles, currentCaselles);
	}

	private bool areEqual(byte[]a, byte[]b){
		for (int i = 0; i < a.Length; i++) {
			if (a [i] != b [i])
				return false;
		}

		return true;
	
	}

	private int getFitxaId(){
		for (int i = 0; i < tempCaselles.Length; i++) {
			if (tempCaselles [i] == FITXA && currentCaselles [i] != FITXA)
				return i;
		}

		return -1;
	}

	private void copy(byte[]origin, byte[]dest){
		for (int i = 0; i < origin.Length; i++) {
			dest [i] = origin [i];
		}
	}

	/*
	private void emptyBoardDetected(){
		if (mDetection.howManyDetected () == 9) {
			checkDetected ();
		} else {
			okCounter = 0;
		}

		if (okCounter == 10) // taulell sencer vist durant 10 frames seguits.
			return true;

		return false;
	}*/

	private byte[]auxiliary = new byte[9];
	private void fillOrderedIds(byte[]a ){
		int fitxaCounter = 0;
		emptyArray (auxiliary);
		//Podem haver detectat més de 9 marcadors.
		for (int i = 0; i < a.Length; i++) {
			int casella = getIndexFromId (a [i], ref fitxaCounter);
			if (casella != -1) {
				//si no entra en algun id, aquella casella quedarà a 0
				auxiliary [casella] = a[i];
			}
		}

		copy (auxiliary, a);

		//omplim caselles a 0 amb tantes fitxes com hem detectat
		int c = 0;
		for (int j = 0; j < a.Length && c < fitxaCounter; j++) {
			if (a [j] == 0) {
				a [j] = FITXA;
				c++;
			}
		}
	} 

	/*private void resetFoundCaselles(){
		for (int i = 0; i < foundCaselles.Length; i++) {
			foundCaselles [i] = false;
		}	
	}*/

	private int getIndexFromId(byte id, ref int fitxaCounter){
		int casella = -1;

		switch (id) {
		case CASELLA_0:
			casella = 0;
			break;
		case CASELLA_1:
			casella = 1;
			break;
		case CASELLA_2:
			casella = 2;
			break;
		case CASELLA_3:
			casella = 3;
			break;
		case CASELLA_4:
			casella = 4;
			break;
		case CASELLA_5:
			casella = 5;
			break;
		case CASELLA_6:
			casella = 6;
			break;
		case CASELLA_7:
			casella = 7;
			break;
		case CASELLA_8:
			casella = 8;
			break;
		case FITXA:
			fitxaCounter++;
			break;
		default:
			break;
		}

		return casella;
	}

	public void pieceToSquare(int casella){
		Transform arObjsTransform = mDetection.markerSettings [casella].transform.GetChild (0);

		/*var children = new List<GameObject>();
		foreach (Transform child in arObjsTransform) 
			children.Add(child.gameObject);
			
		children.ForEach(child => Destroy(child));*/

		GameObject c = Instantiate (Resources.Load ("Prefabs/PieceGlimpsee"), new Vector3 (0, 0, 0), Quaternion.identity) as GameObject;
		c.transform.SetParent (arObjsTransform);
		c.layer = 8; //AR=8
		mDetection.markerSettings[casella].Init();
		c.transform.localPosition = new Vector3 (0f, 0f, 0f);
	}

	public void reset(){
		for (int i = 0; i < mDetection.markerSettings.Length - 1; i++) { // eliminem ARObj FitxaGlimpsee
			Transform arObjsTransform = mDetection.markerSettings [i].transform.GetChild (0);

			var children = new List<GameObject>();
			foreach (Transform child in arObjsTransform) {
				if (child.name.Contains("PieceGlimpsee"))
					children.Add(child.gameObject);
			}
				

			children.ForEach(child => Destroy(child));
		}
	}

	private void emptyArray(byte[]a){
		for (int i = 0; i < a.Length; i++) {
			a [i] = 0;
		}
	}
}


