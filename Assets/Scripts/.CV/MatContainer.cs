﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OpenCVForUnity;

public class MatContainer : MonoBehaviour
{
	private static MatContainer instance;

	private bool currentUpdated;
	private Mat cMat;
	public Mat currentMat{
		get{ 
			if (!currentUpdated) {
				return originalMat;
			} else {
				return cMat;
			}
		}
		set{
			updateCurrent (value);
		}
	}
	public Mat originalMat;

	public WebCamTexture wcTexture;

	/*public Mat loadedMat;
	public Mat gaussianMat;
	public Mat grayscaleMat;
	public Mat cannyMat;
	public Mat houghCirclesMat;
	public Mat shapesMat;
	public Mat finalMat;*/

	public List<Mat> matList;
	public List<string> titles;

	public int cols;
	public int rows;
	public int total_px;

	public Color32[] colors;

	public List<MatOfPoint> polys;

	private bool circlesAdded;
	private Mat circles;
	public Mat Circles{
		get{
			return circles;
		}
		set{
			if (!circlesAdded)
				circlesAdded = true;
			circles = value;
		}
	}

	public Mat M_8UC1{
		get{ return new Mat (rows, cols, CvType.CV_8UC1);}
	}
	public Mat M_8UC3{
		get{return new Mat (rows, cols, CvType.CV_8UC3);}
	}
	public Mat M_8UC4{
		get{return new Mat (rows, cols, CvType.CV_8UC4);}
	}
	public List<MatOfPoint> M_ListMatOfPoint{
		get{return new List<MatOfPoint> ();}
	}



	void Awake()
	{
		//Check if instance already exists
		if (instance == null)
			instance = this;
	}

	public static MatContainer getInstance(){
		return instance;
	}

	public void setDimensions(int cols, int rows, bool useWebcam){
		if (this.cols != cols || this.rows != rows) {
			this.cols = cols;
			this.rows = rows;
			createAll (useWebcam);
		}

	}

	private void createAll(bool useWebcam){

		this.total_px = cols * rows;
		this.colors = new Color32[total_px];

		//MATRIX CREATION
		/*if (useWebcam) {
			loadedMat = new Mat (rows, cols, CvType.CV_8UC4);
		} else {
			loadedMat = new Mat (rows, cols, CvType.CV_8UC3);
		}*/


		originalMat = new Mat (rows, cols, CvType.CV_8UC3);
		cMat = new Mat (rows, cols, CvType.CV_8UC3);

		/*gaussianMat = new Mat (rows, cols, CvType.CV_8UC3);
		grayscaleMat = new Mat (rows, cols, CvType.CV_8UC1);
		cannyMat = new Mat (rows, cols, CvType.CV_8UC1);
		houghCirclesMat = new Mat (rows, cols, CvType.CV_8UC3);
		shapesMat = new Mat (rows, cols, CvType.CV_8UC3);
		finalMat = new Mat (rows, cols, CvType.CV_8UC3);*/

		matList = new List<Mat> ();
		titles = new List<string> ();

		circles = new Mat (rows, cols, CvType.CV_8UC3);
		polys = new List<MatOfPoint> ();

		currentUpdated = false;
		circlesAdded = false;
	}

	public void resetAll(){

		if (circlesAdded) {
			Scalar s = new Scalar (0);
			circles.setTo (s);
			circlesAdded = false;
		}

		/*loadedMat.setTo (s);
		gaussianMat.setTo (s);
		cannyMat.setTo(s);
		houghCirclesMat.setTo(s);
		shapesMat.setTo (s);
		finalMat.setTo (s);*/

		/*matList.Clear ();
		titles.Clear ();*/

		polys.Clear ();
		currentUpdated = false;
	}

	/*public void prepareTodraw(){
		loadedMat.copyTo (finalMat);
	}*/

	/*public void updateStruct(Mat x, string t){
		matList.Add (x);
		titles.Add (t);
		currentMat = x;
	}*/
		

	/*void Start(){
		setDimensions (16, 16);
		createAll ();
	}*/

	/*void Update(){
		Debug.Log ("holii");
	}*/

	public void updateCurrent(Mat m){
		if (!currentUpdated) {
			currentUpdated = true;
			originalMat.copyTo (currentMat);
		}
			
		cMat = m;
		
	}

	/*public void addMatToShow(Mat m, string t){
		matList.Add (m);
		titles.Add (t);
	}*/

	public void setOriginal(Mat m){
		this.originalMat = m;
	}

}

