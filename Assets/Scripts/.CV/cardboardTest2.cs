﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OpenCVMarkerBasedAR;
using MarkerBasedARSample;
using UnityEngine.UI;

using OpenCVForUnity;

public class cardboardTest2 : MonoBehaviour
{
	public InitialSettings m_initialSettings;
	private WebCamTexture webCamTexture;

	public Camera ARCamera;

	public RawImage img;



	void Start(){
		webCamTexture = m_initialSettings.getTexture ();
		Texture2D texture = new Texture2D (webCamTexture.width, webCamTexture.height, TextureFormat.RGBA32, false);

		//img.GetComponent<RawImage>().texture = texture;

		/*gameObject.transform.localScale = new Vector3 (webCamTexture.width, webCamTexture.height, 1);

		float width = 0;
		float height = 0;

		width = gameObject.transform.localScale.x;
		height = gameObject.transform.localScale.y;

		float imageScale = 1.0f;
		float widthScale = (float)Screen.width / width;
		float heightScale = (float)Screen.height / height;
		if (widthScale < heightScale) {
			Camera.main.orthographicSize = height / 2;
		} else {
			Camera.main.orthographicSize = (width * (float)Screen.height / (float)Screen.width) / 2;
			imageScale = (float)Screen.height / (float)Screen.width;
		}

		gameObject.GetComponent<Renderer> ().material.mainTexture = texture;

		//set cameraparam
		int max_d = Mathf.Max (webCamTexture.width, webCamTexture.height);
		Mat camMatrix = new Mat (3, 3, CvType.CV_64FC1);
		camMatrix.put (0, 0, max_d);
		camMatrix.put (0, 1, 0);
		camMatrix.put (0, 2, webCamTexture.width / 2.0f);
		camMatrix.put (1, 0, 0);
		camMatrix.put (1, 1, max_d);
		camMatrix.put (1, 2, webCamTexture.height / 2.0f);
		camMatrix.put (2, 0, 0);
		camMatrix.put (2, 1, 0);
		camMatrix.put (2, 2, 1.0f);
		Debug.Log ("camMatrix " + camMatrix.dump ());

		MatOfDouble distCoeffs = new MatOfDouble (0, 0, 0, 0);
		Debug.Log ("distCoeffs " + distCoeffs.dump ());

		//calibration camera
		Size imageSize = new Size (webCamTexture.width * imageScale, webCamTexture.height * imageScale);
		double apertureWidth = 0;
		double apertureHeight = 0;
		double[] fovx = new double[1];
		double[] fovy = new double[1];
		double[] focalLength = new double[1];
		Point principalPoint = new Point ();
		double[] aspectratio = new double[1];


		Calib3d.calibrationMatrixValues (camMatrix, imageSize, apertureWidth, apertureHeight, fovx, fovy, focalLength, principalPoint, aspectratio);

		Debug.Log ("imageSize " + imageSize.ToString ());
		Debug.Log ("apertureWidth " + apertureWidth);
		Debug.Log ("apertureHeight " + apertureHeight);
		Debug.Log ("fovx " + fovx [0]);
		Debug.Log ("fovy " + fovy [0]);
		Debug.Log ("focalLength " + focalLength [0]);
		Debug.Log ("principalPoint " + principalPoint.ToString ());
		Debug.Log ("aspectratio " + aspectratio [0]);

		//Adjust Unity Camera FOV
		if (widthScale < heightScale) {
			ARCamera.fieldOfView = (float)fovy[0];
		} else {
			ARCamera.fieldOfView = (float)fovx[0];
		}*/



		/*MarkerDesign[] markerDesigns = new MarkerDesign[markerSettings.Length];
		for (int i = 0; i < markerDesigns.Length; i++) {
			markerDesigns [i] = markerSettings [i].markerDesign;
		}							

		markerDetector = new MarkerDetector (camMatrix, distCoeffs, markerDesigns);



		invertYM = Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3 (1, -1, 1));
		Debug.Log ("invertYM " + invertYM.ToString ());

		invertZM = Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3 (1, 1, -1));
		Debug.Log ("invertZM " + invertZM.ToString ());*/
	}

	void Update(){
		Mat m = m_initialSettings.retrieveInitial ();
		Texture2D texture = new Texture2D (m.cols (), m.rows (), TextureFormat.RGBA32, false);
		Utils.matToTexture2D (m, texture);
		this.gameObject.GetComponent<Renderer> ().material.mainTexture = texture;
		//img.GetComponent<RawImage> ().texture = texture;
	}

}