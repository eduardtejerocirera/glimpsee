﻿using UnityEngine;
using System.Collections;
using OpenCVForUnity;

public class Looper : MonoBehaviour
{

	//A const obj is ALWAYS static

	//STATES
	//0 means stop until next frame
	private const byte RETRIEVE = 1;
	private const byte GRAYSCALE = 2;
	private const byte HOUGHCIRCLES = 4;
	private const byte SHOWIMAGES = 5;
	private const byte RESETMAT = 6;
	private const byte GENERATEFINAL = 7;
	private const byte INRANGE_BLUE = 8;
	private const byte INRANGE_RED = 9;
	private const byte INRANGE_YELLOW = 10;
	private const byte INRANGE_GREEN = 11;
	private const byte FINDCONTOURS = 12; // ALL
	private const byte FINDTRIANGLE = 15;
	private const byte FINDQUAD = 16;
	private const byte FINDPOLY = 17;
	private const byte MARKERS = 13;
	private const byte MARKERS_TICTACTOE = 18;
	private const byte VUFORIA_TICTACTOE = 19;

	//PROGRAMS
	/*public const byte PGM_DONOTHING = 0;
	public const byte PGM_SHAPEDETECTION_CANNY = 1;
	public const byte PGM_TRIANGLEDETECTION_BLUE = 2;
	public const byte PGM_MARKER = 3;*/

	//PROGRAMS strings
	public const string PGM_DONOTHING = "Mixed Reality";
	public const string PGM_SHAPEDETECTION = "Shape Detection";
	public const string PGM_TRIANGLEDETECTION_BLUE = "Blue Triangles";
	public const string PGM_MARKER = "Marker Detection";
	public const string PGM_MARKER_TICTACTOE = "Tic Tac Toe";

	public static readonly string[] programs = {PGM_DONOTHING, PGM_SHAPEDETECTION, PGM_TRIANGLEDETECTION_BLUE, PGM_MARKER, 
		PGM_MARKER_TICTACTOE};

	private byte[] myStates;
	//private byte[] myStates = { RETRIEVE, GRAYSCALE, FINDCONTOURS, SHOWIMAGES, RESETMAT};

	//private byte[] myStates = { RETRIEVE, GRAYSCALE, FINDCONTOURS_CANNY, SHOWIMAGES, RESETMAT};
	//private byte[] myStates = { RETRIEVE, MARKERS, SHOWIMAGES, RESETMAT};


	private int totalLength;
	private int ind;

	private InitialSettings m_initialSettings;

	void Start ()
	{
		m_initialSettings = this.gameObject.GetComponent<InitialSettings> ();

		loadProgram (PGM_DONOTHING, false); //by default we load the donothing pgm.
		totalLength = myStates.Length;
		ind = 0;
	}

	private float t0 = 0;
	private float t1 = 0;
	void Update ()
	{
		//t0 = Time.realtimeSinceStartup;
		ind = ind % totalLength;

		while (ind < totalLength) {

			if (myStates [ind] == 0) {
				ind++;
				break;
			}

			SelectState (myStates [ind]);
			ind++;
		}

		//t1 = Time.realtimeSinceStartup;

	}

	private void SelectState(byte state){
		switch (state) {
		case 0://BREAK
			Debug.Log ("breaking, I shouldn't be here");
			break;
		case RETRIEVE:
			MatContainer.getInstance ().setOriginal (m_initialSettings.retrieveInitial ());
			prepareToPrint ("original");
			break;
		case GRAYSCALE://GRAYSCALE
		case INRANGE_BLUE:
		case INRANGE_GREEN:
		case INRANGE_RED:
		case INRANGE_YELLOW:
		
			prepareINRANGE (state);
			if (state == GRAYSCALE) {
				prepareToPrint ("grayscale");
			} else {
				prepareToPrint ("inrange");
			}

			break;
		case HOUGHCIRCLES://HOUGH
			MatContainer.getInstance ().Circles = ImageOperations.getInstance ().HOUGH (MatContainer.getInstance ().currentMat);
			ImgShowCanvas.getInstance ().generateCircleShapesMat (MatContainer.getInstance().Circles, m_initialSettings.showAll);
			break;
		case SHOWIMAGES://SHOWIM
			ImgShowCanvas.getInstance ().imshow (m_initialSettings.isCardboard);
			break;
		case RESETMAT://RESET
			ImgShowCanvas.getInstance ().resetMatList ();
			MatContainer.getInstance ().resetAll ();
			break;
		case GENERATEFINAL:
			ImgShowCanvas.getInstance ().prepareToPrintFinal ();
			break;
		case FINDCONTOURS:
		case FINDTRIANGLE:
		case FINDQUAD: 
		case FINDPOLY:
			MatContainer.getInstance ().polys = ImageOperations.getInstance ().FINDCONTOURS (MatContainer.getInstance ().currentMat);
			prepareFindContours (state);
			break;
		case MARKERS:
			Mat c = SceneObjects.getInstance ().MarkerDetecScript.processMarker (MatContainer.getInstance ().currentMat);
			if (c != null)
				MatContainer.getInstance ().currentMat = c;
			prepareToPrint ("Markers");
			break;
		case MARKERS_TICTACTOE:
			SceneObjects.getInstance ().MarkTicTacToeScript.analiseMarkers (MatContainer.getInstance ().currentMat);
			break;
		case VUFORIA_TICTACTOE:
			break;
		default:
			Debug.Log ("I shouldn't be here");
			break;

		}
	
	}

	private void prepareToPrint(string t){
		if (m_initialSettings.showAll)
			ImgShowCanvas.getInstance ().addMatAndTitle (MatContainer.getInstance ().currentMat, t);
	}

	/*public void loadProgram(byte pgm, bool reset = true){

		if (reset) {
			SelectState (RESETMAT); //reset everything before starting a new pgm.
			if (!m_initialSettings.isCardboard) 
				ImgShowCanvas.getInstance().ClearCanvas();
		}
			

		switch (pgm) {
		case PGM_DONOTHING:
			myStates = new byte[] { RETRIEVE, SHOWIMAGES, RESETMAT };
			break;
		case PGM_SHAPEDETECTION_CANNY:
			myStates = new byte[] { RETRIEVE, GRAYSCALE, HOUGHCIRCLES, SHOWIMAGES, 0,
				FINDCONTOURS, SHOWIMAGES, RESETMAT, 0};
			break;
		case PGM_TRIANGLEDETECTION_BLUE:
			myStates = new byte[] { RETRIEVE, INRANGE_BLUE, FINDTRIANGLE, SHOWIMAGES, RESETMAT };
			break;
		case PGM_MARKER:
			myStates = new byte[] { RETRIEVE, MARKERS, SHOWIMAGES, RESETMAT };
			break;
		default:
			Debug.Log ("I shouldn't be here");
			break;
		}

		totalLength = myStates.Length;
		ind = 0;
	}*/

	public void loadProgram(string pgm, bool reset = true){

		if (reset) {
			SelectState (RESETMAT); //reset everything before starting a new pgm.
			if (!m_initialSettings.isCardboard) 
				ImgShowCanvas.getInstance().ClearCanvas();
		}

		if (pgm == PGM_DONOTHING) {
			myStates = new byte[] { RETRIEVE, SHOWIMAGES, RESETMAT };
		} else if (pgm == PGM_SHAPEDETECTION) {
			myStates = new byte[] { RETRIEVE, GRAYSCALE, HOUGHCIRCLES, SHOWIMAGES, 0,
				FINDCONTOURS, SHOWIMAGES, RESETMAT, 0
			};
		} else if (pgm == PGM_TRIANGLEDETECTION_BLUE) {
			myStates = new byte[] { RETRIEVE, INRANGE_BLUE, FINDTRIANGLE, SHOWIMAGES, RESETMAT };
		} else if (pgm == PGM_MARKER) {
			myStates = new byte[] { RETRIEVE, MARKERS, SHOWIMAGES, RESETMAT };
		} else if(pgm == PGM_MARKER_TICTACTOE){
			myStates = new byte[] { RETRIEVE, MARKERS_TICTACTOE, SHOWIMAGES, RESETMAT };
		}else{
			Debug.Log ("I shouldn't be here");
		}

		totalLength = myStates.Length;
		ind = 0;
	}





	private void prepareINRANGE(byte inRange){
		string col;

		switch (inRange) {
		case INRANGE_BLUE:
			col = "blue";
			break;
		case INRANGE_GREEN:
			col = "green";
			break;
		case INRANGE_RED:
			col = "red";
			break;
		case INRANGE_YELLOW:
			col = "yellow";
			break;
		default:
			col = ""; //GRAYSCALE, not inRange
			break;
		}

		if (col != "") {
			MatContainer.getInstance ().currentMat = 
				ImageOperations.getInstance ().INRANGE (MatContainer.getInstance ().currentMat, col);
		} else {
			MatContainer.getInstance ().currentMat = ImageOperations.getInstance ().GRAYSCALE (MatContainer.getInstance ().currentMat);
		}
	}

	private void prepareFindContours(byte shape){
		int reqEdges = 0;

		switch (shape) {
		case FINDCONTOURS:
			reqEdges = -1;
			break;
		case FINDTRIANGLE:
			reqEdges = 3;
			break;
		case FINDQUAD:
			reqEdges = 4;
			break;
		case FINDPOLY:
			reqEdges = 5;
			break;
		default:
			Debug.Log ("I shouldn't be here");
			break;
		}

		ImgShowCanvas.getInstance ().
		generatePolyShapesMat (MatContainer.getInstance ().polys, m_initialSettings.showAll, reqEdges);
	
	}
}

