﻿using UnityEngine;
using System.Collections;
using OpenCVMarkerBasedAR;
using System.Collections.Generic;

public class SceneObjects : MonoBehaviour
{
	private static SceneObjects instance;


	public GameObject Main;
	public GameObject CardBoardMain;
	public GameObject Canvas;
	public GameObject CanvasImages;
	public GameObject MarkerDetecObject;
	public GameObject GameLogic;

	public List<MarkerSettings> MarkSettings;

	public InitialSettings InitSettings;
	//public MarkerDetection MarkerDetecScript;
	public MarkerDetection MarkerDetecScript;
	public Looper Looper;
	public MarkerTictactoe MarkTicTacToeScript;
	public MarkerTictactoeController mtController;
	public Camera ARCamera;

	public GameObject ArQuad;
	public GameObject OutQuad;



	void Awake(){
		instance = this;
		Main = GameObject.Find ("Main");
		CardBoardMain = GameObject.Find ("CardboardMain");
		Canvas = GameObject.Find ("Canvas");
		CanvasImages = GameObject.Find ("CanvasImages");
		MarkerDetecObject = GameObject.Find ("MarkerDetection");
		GameLogic = GameObject.Find ("GameLogic");
		ARCamera = GameObject.Find ("ARCamera").GetComponent<Camera>();

		ArQuad = GameObject.Find ("ARQuad");
		ArQuad.SetActive (false);

		OutQuad = GameObject.Find ("OutputQuad");
		OutQuad.SetActive (false);



	
		/*MarkSettings = new List<MarkerSettings> ();
		for (int i = 1; i < MarkerDetecObject.transform.childCount; i++) {
			MarkerSettings m = MarkerDetecObject.transform.GetChild (i).GetComponent<MarkerSettings> ();
			MarkSettings.Add (m);
		}*/

		MarkTicTacToeScript = Main.GetComponent<MarkerTictactoe> ();
		Looper = Main.GetComponent<Looper>();
		InitSettings = Main.GetComponent<InitialSettings> ();
		//MarkerDetecScript = MainObject_Quad.GetComponent<MarkerDetection> ();
		MarkerDetecScript = ArQuad.GetComponent<MarkerDetection> ();

		mtController = GameLogic.GetComponent<MarkerTictactoeController> ();

		GameLogic.SetActive (false);
		//MarkerDetecObject.transform.GetChild(0).gameObject.SetActive (false);

	
	}

	public static SceneObjects getInstance(){
		return instance;
	}

}

