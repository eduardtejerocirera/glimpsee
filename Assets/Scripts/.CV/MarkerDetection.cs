﻿using UnityEngine;
using System.Collections;
using OpenCVMarkerBasedAR;
using OpenCVForUnity;
using System.Collections.Generic;
using MarkerBasedARSample;



public class MarkerDetection : MonoBehaviour
{
	public Camera ARCamera; 

	public MarkerSettings[] markerSettings;

	public MarkerDetector markerDetector;

	private Matrix4x4 transformationM;
	private Mat camMatrix;
	private MatOfDouble distCoeffs;

	private Mat webCamTextureMat;
	private Matrix4x4 invertZM;
	private Matrix4x4 invertYM;
	private Matrix4x4 ARM;

	Color32[] colors;
	Texture2D texture;
	Quaternion currentARCameraLocalRotation;

	private InitialSettings m_initialSettings;

	void Start(){
		m_initialSettings = SceneObjects.getInstance ().InitSettings;
		init (m_initialSettings.retrieveInitial());
	}



	private void addTextureWebcam(){
		colors = new Color32[webCamTextureMat.cols () * webCamTextureMat.rows ()];
		texture = new Texture2D (webCamTextureMat.cols (), webCamTextureMat.rows (), TextureFormat.RGBA32, false);
		gameObject.transform.localScale = new Vector3 (webCamTextureMat.cols (), webCamTextureMat.rows (), 1);
		gameObject.GetComponent<Renderer> ().material.mainTexture = texture;
		//Debug.Log ("Screen.width " + Screen.width + " Screen.height " + Screen.height + " Screen.orientation " + Screen.orientation);
	}

	private void adaptCameraToScreen(){
		widthScale = (float)Screen.width / width;
		heightScale = (float)Screen.height / height;

		if (widthScale < heightScale) {
			Camera.main.orthographicSize = height / 2;
		} else {
			Camera.main.orthographicSize = (width * (float)Screen.height / (float)Screen.width) / 2;
			imageScale = (float)Screen.height / (float)Screen.width;
		}
	}

	private void prepareCamMatrix(){
		int max_d = Mathf.Max (webCamTextureMat.rows (), webCamTextureMat.cols ());
		camMatrix = new Mat (3, 3, CvType.CV_64FC1);
		camMatrix.put (0, 0, max_d);
		camMatrix.put (0, 1, 0);
		camMatrix.put (0, 2, webCamTextureMat.cols () / 2.0f);
		camMatrix.put (1, 0, 0);
		camMatrix.put (1, 1, max_d);
		camMatrix.put (1, 2, webCamTextureMat.rows () / 2.0f);
		camMatrix.put (2, 0, 0);
		camMatrix.put (2, 1, 0);
		camMatrix.put (2, 2, 1.0f);
		Debug.Log ("camMatrix " + camMatrix.dump ());

		distCoeffs = new MatOfDouble (0, 0, 0, 0);
		Debug.Log ("distCoeffs " + distCoeffs.dump ());
	}

	private void calibrateCam(){
		Size imageSize = new Size (webCamTextureMat.cols () * imageScale, webCamTextureMat.rows () * imageScale);
		double apertureWidth = 0;
		double apertureHeight = 0;
		double[] fovx = new double[1];
		double[] fovy = new double[1];
		double[] focalLength = new double[1];
		Point principalPoint = new Point ();
		double[] aspectratio = new double[1];


		Calib3d.calibrationMatrixValues (camMatrix, imageSize, apertureWidth, apertureHeight, fovx, fovy, focalLength, principalPoint, aspectratio);

		//Debug.Log ("imageSize " + imageSize.ToString ());
		//Debug.Log ("apertureWidth " + apertureWidth);
		//Debug.Log ("apertureHeight " + apertureHeight);
		//Debug.Log ("fovx " + fovx [0]);
		//Debug.Log ("fovy " + fovy [0]);
		//Debug.Log ("focalLength " + focalLength [0]);
		//Debug.Log ("principalPoint " + principalPoint.ToString ());
		//Debug.Log ("aspectratio " + aspectratio [0]);

		//Adjust Unity Camera FOV
		if (widthScale < heightScale) {
			ARCamera.fieldOfView = (float)fovy[0];
		} else {
			ARCamera.fieldOfView = (float)fovx[0];
		}
	}

	private void prepareMarkers(){
		MarkerDesign[] markerDesigns = new MarkerDesign[markerSettings.Length];
		for (int i = 0; i < markerDesigns.Length; i++) {
			markerDesigns [i] = markerSettings [i].markerDesign;
		}							

		markerDetector = new MarkerDetector (camMatrix, distCoeffs, markerDesigns);
	}

	private void prepareInvertMatrix(){
		invertYM = Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3 (1, -1, 1));
		Debug.Log ("invertYM " + invertYM.ToString ());

		invertZM = Matrix4x4.TRS (Vector3.zero, Quaternion.identity, new Vector3 (1, 1, -1));
		Debug.Log ("invertZM " + invertZM.ToString ());
	}


	private float width=0, height=0, imageScale=0, widthScale=0, heightScale=0;

	private void init (Mat m)
	{
		webCamTextureMat = m;

		addTextureWebcam ();

		width = gameObject.transform.localScale.x;
		height = gameObject.transform.localScale.y;
		imageScale = 1.0f;


		adaptCameraToScreen ();
		prepareCamMatrix ();
		calibrateCam ();
		ARCamera.fieldOfView = 63f;
		prepareMarkers ();
		prepareInvertMatrix ();
	}

		
	public Mat processMarker (Mat m, out int numMarkers)
	{

		SceneObjects.getInstance ().MarkerDetecObject.SetActive (true);
		Mat outMat = null;

		//for frontFaceingCamera
		//Core.flip (rgbaMat, rgbaMat, 1);

		//markerDetector guarda en una llista interna els marcadors
		markerDetector.processFrame(m, 1); 

		//Es desactiven tots els ARObjects
		foreach (MarkerSettings settings in markerSettings)
		{
			settings.setAllARGameObjectsDisable();
		}
			

		List<Marker> findMarkers = markerDetector.getFindMarkers();
		numMarkers = findMarkers.Count;
		for (int i = 0; i < numMarkers; i++)
		{
			Marker marker = findMarkers[i];

			foreach (MarkerSettings settings in markerSettings)
			{
				//Per cada marcador trobat per markerDetector, es comprova si coincideix amb algun
				//dels ids determinats a markerSettings
				if (marker.id == settings.getMarkerId())
				{

					transformationM = marker.transformation;
					ARM = ARCamera.transform.localToWorldMatrix * invertYM * transformationM * invertZM;


					//S'activen els ARObjects pertinents i s'adapten a la càmera.
					GameObject ARGameObject = settings.getARGameObject();
					if (ARGameObject != null)
					{
						ARUtils.SetTransformFromMatrix(ARGameObject.transform, ref ARM);
						ARGameObject.SetActive(true);
					}


					//dibuixem
					if (!m_initialSettings.isCardboard) {
						outMat = MatContainer.getInstance ().M_8UC3;
						m.copyTo (outMat);
						marker.drawContour (outMat, new Scalar (0, 255, 0));
					}// else {
					//	outMat = m;
					//}
						
				}
			}
		}

		//desactivem el gameobject que conté la detecció de marcadors, perquè sino dona errors.
		//SceneObjects.getInstance ().MarkerDetecObject.SetActive (false);
		return outMat;
	}

	public int howManyDetected(Mat m){
		int numMarkers;
		processMarker (m, out numMarkers);
		//Debug.Log("Num of markers: " + markerDetector.getFindMarkers().Count);
		return numMarkers;
	}

	public Mat processMarker (Mat m)
	{


		SceneObjects.getInstance ().MarkerDetecObject.SetActive (true);
		Mat outMat = null;

		//for frontFaceingCamera
		//Core.flip (rgbaMat, rgbaMat, 1);

		//markerDetector guarda en una llista interna els marcadors
		markerDetector.processFrame(m, 1); 

		//Es desactiven tots els ARObjects
		foreach (MarkerSettings settings in markerSettings)
		{
			settings.setAllARGameObjectsDisable();
		}


		List<Marker> findMarkers = markerDetector.getFindMarkers();

		for (int i = 0; i < findMarkers.Count; i++)
		{
			Marker marker = findMarkers[i];

			foreach (MarkerSettings settings in markerSettings)
			{
				//Per cada marcador trobat per markerDetector, es comprova si coincideix amb algun
				//dels ids determinats a markerSettings
				if (marker.id == settings.getMarkerId())
				{

					transformationM = marker.transformation;
					ARM = ARCamera.transform.localToWorldMatrix * invertYM * transformationM * invertZM;


					//S'activen els ARObjects pertinents i s'adapten a la càmera.
					GameObject ARGameObject = settings.getARGameObject();
					if (ARGameObject != null)
					{
						ARUtils.SetTransformFromMatrix(ARGameObject.transform, ref ARM);
						ARGameObject.SetActive(true);
					}


					//dibuixem
					if (!m_initialSettings.isCardboard) {
						outMat = MatContainer.getInstance ().M_8UC3;
						m.copyTo (outMat);
						marker.drawContour (outMat, new Scalar (0, 255, 0));
					}// else {
					//	outMat = m;
					//}

				}
			}
		}

		//desactivem el gameobject que conté la detecció de marcadors, perquè sino dona errors.
		//SceneObjects.getInstance ().MarkerDetecObject.SetActive (false);
		return outMat;
	}



	/*public Mat processMarker(Mat m){
		
		Mat rgbaMat = m;

			//for frontFaceingCamera
			//Core.flip (rgbaMat, rgbaMat, 1);

		markerDetector.processFrame(rgbaMat, 1);


			foreach (MarkerSettings settings in markerSettings)
			{
				settings.setAllARGameObjectsDisable();
			}

			List<Marker> findMarkers = markerDetector.getFindMarkers();
			for (int i = 0; i < findMarkers.Count; i++)
			{
				Marker marker = findMarkers[i];

				foreach (MarkerSettings settings in markerSettings)
				{
					if (marker.id == settings.getMarkerId())
					{
						transformationM = marker.transformation;
						//Debug.Log ("transformationM " + transformationM.ToString ());

						ARM = ARCamera.transform.localToWorldMatrix * invertYM * transformationM * invertZM;
						//Debug.Log ("arM " + arM.ToString ());


						GameObject ARGameObject = settings.getARGameObject();
						if (ARGameObject != null)
						{
							ARUtils.SetTransformFromMatrix(ARGameObject.transform, ref ARM);
							ARGameObject.SetActive(true);
						}
					}
				}
			}

			Utils.matToTexture2D(rgbaMat, texture, colors);


			currentARCameraLocalRotation = ARCamera.transform.localRotation;

		ARCamera.transform.localRotation = currentARCameraLocalRotation;
		return rgbaMat;
	}*/
}

