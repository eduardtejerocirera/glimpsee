﻿using UnityEngine;
using System.Collections;

public class DistractorHandler : MonoBehaviour
{

	/*private string[] distractorNames = new string[72]{
		"cube","anarchy","clover","batman","mickey","sword","ying","alpha","music","glasses",
		"fish","pi","peace","umbrella","bird","trophy","trash","drop","clock","phone",
		"sun","star","smile","shop","crosshair","wifi","refresh","question","puzzle","power",
		"plane","clip","lock","headphones","cross","micro","man","loupe","lightning","bulb",
		"lemon","home","heart","game","cloud","eye","euro","earth","envelope","dropbox",
		"ball","dollar","scissors","comment","engine","coffee","circle","bullseye","bug","bell",
		"ban","apple","flower","&","@","%","battery","diamond","car","key",
		"eject","#"};*/

	public GameObject partner;
	private int id;
	private MotionManager mManager;
	private int numTotalDistractors;

	private GameObject[] distractors;
	private int currD;
	private bool stopRunning = false;

	public void Init ()
	{
		stopRunning = false;
		mManager = new MotionManager ();

		id = int.Parse (this.gameObject.name.Substring (1));
		int partId = (id % 2 == 0) ? id + 1 : id - 1;
		partner = GameObject.Find ("P" + partId).gameObject;

		mManager.prepareMRU (this.transform.position, partner.transform.position,
			CentralData.getInstance ().Tdistractor, false);

		numTotalDistractors = CentralData.getInstance ().icons.Length;

		distractors = new GameObject[5];
		for (int i = 0; i < distractors.Length; i++) {
			distractors[i] = (GameObject)Instantiate(Resources.Load("Prefabs/Distractor"));
			distractors [i].SetActive (false);
		}
	}

	public void triggerDistractor(int ind){
		if (ind < numTotalDistractors)
			StartCoroutine (sendDistractor (ind));
	}

	IEnumerator sendDistractor(int ind){
		stopRunning = false;

		GameObject s = distractors [currD];
		currD++;
		currD = currD % distractors.Length;

		s.transform.position = this.transform.position;

		Texture2D img = (Texture2D)CentralData.getInstance ().getIconIndex (ind);
		setTextureToDistractor (s, img);

		s.SetActive (true);

		mManager.initialMovement (s.transform.position, partner.transform.position);

		while (	!mManager.withinSphereBounds(s.transform.position, partner.transform.position) && !stopRunning ){
			s.transform.position = mManager.computeNewPos(s.transform.position);
			yield return 0;
		}

		s.SetActive (false);

	}

	private void setTextureToDistractor(GameObject s, Texture2D img){
		s.transform.GetChild(0).GetComponent<Renderer> ().material.mainTexture = img;
	}

	public void cheatingListener(){
		stopRunning = true;
	}

}

