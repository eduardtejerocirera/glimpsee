﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonkeyEthan : MonoBehaviour {

	private bool walk = false;
	private Animator animator;
	//private Vector3[] points;
	public List<Vector3> points = new List<Vector3>();
	private int numPoints = 2;
	private int currP = 0;

	public bool Walk{
		set{
			walk = value;
			animator.SetBool ("Walk", value);

			if (value)
				startMoving ();
		}
		get{
			return walk; 
		}
	}



	void Awake(){
		//FALTA ARREGLAR: isFinal s'ha de posar a false a l'ultim point
		int i = 0;
		bool yes = true;
		Transform lastAux = null;
		while (yes) {
			Transform aux = this.transform.parent.Find ("P" + i.ToString ());
			if (aux == null) {
				yes = false;
			} else {
				points.Add (aux.transform.position);
				aux.gameObject.GetComponent<MonkeyWalkCollider> ().Init (this, false);
				lastAux = aux;
			}
			i++;

		}

		lastAux.gameObject.GetComponent<MonkeyWalkCollider>().isFinal = true;

		GameObject theMonkey = this.transform.parent.gameObject;
		GameObject.Find ("Logic").GetComponent<MonkeyBusinessController> ().theMonkey = theMonkey;
		theMonkey.SetActive (false);
	}

	void Start () {
		animator = this.gameObject.GetComponent<Animator> ();
	}

	private void startMoving(){
		StartCoroutine (ethanMove ());
	}
	
	IEnumerator ethanMove(){
		prepareComputePosition ();

		while (walk) {
			computePosition ();
			yield return 0;
		}

		currP++;
		
	}

	private Vector3 dir;
	private float incr = 0.1f;

	private void prepareComputePosition(){
		dir = points [currP + 1] - points [currP];
		dir.Normalize ();

		this.transform.rotation = Quaternion.LookRotation (dir);
	}

	private void computePosition(){
		this.transform.position += dir * incr;
	}


}
