﻿using UnityEngine;
using System.Collections;

public class MonkeyBusinessController : ControllerBase {

	// Use this for initialization

	public GameObject[] spheres;
	public GameObject[] particles;
	public GameObject[] distractorsSources;
	public voiceCoroutines vCoroutines;
	public GameObject theMonkey;

	private int helpCounter;



	public float[] initialSpeed = { 0.05f, 0.5f };
	public bool[] withMonkey = { false, true };

	private MonkeyBusinessLogic mbLogic;

	public GlimpseeVoice gVoice;

	private int numParticles;
	private int finishedCoroutines = 0;
	private int numDistractors;
	private int numSpheres;

	private int currDistractorSource = 0;
	private int indDistractor = 0;
	private int distCounter = 0;

	void Start(){
		numParticles = CentralData.getInstance ().numTeamsMB;
		numSpheres = CentralData.getInstance ().numSpheresMB;
		numDistractors = CentralData.getInstance ().numDistractorsSources;
		GameObject.Find ("Logic").GetComponent<VuforiaMonkeyBusiness> ().Init (this, numSpheres);
		CentralData.CheatingEvent += cheatingListener;
	}

	public override void Init () {

		spheres = new GameObject[numSpheres];
		for (int i = 0; i < spheres.Length; i++) {
			spheres [i] = GameObject.Find ("Sphere" + i).gameObject;
			char c = (i%2==0)? 'A' : '#';
			changeTextureOfSphere (c,i);
		}

		distractorsSources = new GameObject[numDistractors];
		for (int i = 0; i < numDistractors; i++) {
			distractorsSources [i] = GameObject.Find ("P" + i).gameObject;
			distractorsSources [i].GetComponent<DistractorHandler> ().Init ();
		}



		//vCoroutines = GameObject.Find ("Countdown_UI").gameObject.GetComponent<voiceCoroutines> ();

		gVoice = GameObject.Find ("GlimpseeVoice").GetComponent<GlimpseeVoice> ();
		gVoice.Load (false);

		mbLogic = this.gameObject.GetComponent<MonkeyBusinessLogic> ();
		mbLogic.Create (numSpheres, numParticles);

		vCoroutines = this.gameObject.GetComponent<voiceCoroutines> ();

		createParticles ();




		vCoroutines.Init (this);
		vCoroutines.t1_sayIntro (mbLogic.level);

	}

	private void createParticles(){
		particles = new GameObject[numParticles];
		//particleVel = new Vector3[numParticles];
		GameObject particlePrefab = (GameObject) Resources.Load ("Prefabs/ParticleMonkey", typeof(GameObject));

		for (int i = 0; i < numParticles; i++) {
			particles [i] = (GameObject)Instantiate(particlePrefab, new Vector3(i * 2.0f + 1f, 2.0f, 0), Quaternion.identity);
			particles [i].GetComponent<particleScript> ().Create(i, this, mbLogic.teams[i].order);

			/*if (i == 0) {
				particles [i].GetComponent<Renderer> ().material.color = Color.green;
			} else {
				particles [i].GetComponent<Renderer> ().material.color = new Color(Random.Range(0.0f,1.0f),Random.Range(0.0f,0.5f),Random.Range(0.0f,1.0f));
			}*/
		}
	}
		
		
	public void startNewLevel(){ //Called after intro
		helpCounter = 0;
		indDistractor = 0;
		distCounter = 0;

		mbLogic.Init ();
		mbLogic.fillSpheres ();
		setParticlesStartPosition ();
		initTextures ();
		vCoroutines.t1_startCountdown (mbLogic.level);
	}

	public void initAndStart(){
		
		initParticleMovement ();
		//triggerDistractor ();
	}

	public void initParticleMovement(){
		
		for (int i = 0; i < particles.Length; i++) {
			//StartCoroutine (particleMove (i, -1, 0));
			particles[i].GetComponent<particleScript>().Init(mbLogic.teams[i].totalChars, mbLogic.level);
			particles[i].GetComponent<particleScript>().initParticleMovent(mbLogic.level == 0);
		}

		//if (withMonkey [mbLogic.level])
		//	theMonkey.SetActive (true);

	}

	public void setParticlesStartPosition(){
		for (int i = 0; i < particles.Length; i++) {
			particles [i].GetComponent<particleScript> ().setInitialPos ();
		}
	}

	private void initTextures(){
		for (int i = 0; i < spheres.Length; i++) {
			string texName = spheres [i].GetComponent<SphereMonkey>().Tex.name;
			char c = mbLogic.spheres [i];

			if (c.ToString () != texName) {
				Texture2D img = CentralData.getInstance ().getTextureLetter (char.ToUpper(c));
				//Texture2D img = (Texture2D) Instantiate( Resources.Load("Img/alphabetLetters/" + c, typeof(Texture2D)) );
				spheres [i].GetComponent<SphereMonkey>().Tex = img;
			}
		}
	}

	public void changeTextureOfSphere(int index){
		char c = mbLogic.spheres [index];
		Texture2D img = CentralData.getInstance ().getTextureLetter (char.ToUpper(c));
		//Texture2D img = (Texture2D) Instantiate( Resources.Load("Img/alphabetLetters/" + c, typeof(Texture2D)) );
		spheres [index].GetComponent<SphereMonkey>().Tex = img;
	}

	public void changeTextureOfSphere(char c, int i){
		Texture2D img = CentralData.getInstance ().getTextureLetter (char.ToUpper(c));
		//Texture2D img = (Texture2D) Instantiate( Resources.Load("Img/alphabetLetters/" + letter, typeof(Texture2D)) );
		spheres [i].GetComponent<SphereMonkey>().Tex = img;
	}

	/*private float incr;
	private int finishedCoroutines = 0;
	IEnumerator particleMove(int particleIndex, int init, int end){


		int e = mbLogic.orderTeam[particleIndex] [end];
		Vector3 endPos = spheres [e].transform.position;
		Vector3 initPos = new Vector3();

		if (init == -1) {
			initPos = spheres [e].transform.position;
			initPos.x += 2.0f; initPos.y += 2.0f; initPos.z += 2.0f;
		} else {
			int i = mbLogic.orderTeam[particleIndex] [init];
			initPos = spheres [i].transform.position;
		}

		particleVel[particleIndex] = prepareComputePosition (initPos, endPos);
		//Vector3 dir = (endPos - initPos);
		//dir.Normalize ();
		//Vector3 particlePos = particle.transform.position;
		//particlePos = initPos;
		//Vector3 incrVector = makeVectorOfModulus (incr, dir);
		Vector3 particlePos = initPos;
		particles[particleIndex].gameObject.transform.position = particlePos;

		yield return 0;

		while (	!(Mathf.Abs (endPos.x - particlePos.x) <= 0.5 &&
		        Mathf.Abs (endPos.y - particlePos.y) <= 0.5 &&
				Mathf.Abs (endPos.z - particlePos.z) <= 0.5)){

			//particlePos = new Vector3 (particlePos.x + dir.x*incr, particlePos.y + dir.y*incr, particlePos.z + dir.z*incr);
			computePosition(ref particlePos, ref particleVel[particleIndex]);
			particles[particleIndex].gameObject.transform.position = particlePos;
			yield return 0;
		}


		mbLogic.checkNewLetter(e, particleIndex);
		spheres [e].GetComponent<SphereMonkey> ().playCollideAnimation ();

		if (mbLogic.orderTeam [particleIndex].Count - 1 > end) {
			StartCoroutine (particleMove (particleIndex, init + 1, end + 1));
		} else {
			finishedCoroutines++;
			if (finishedCoroutines == particles.Length) {
				finishedCoroutines = 0;
				mbLogic.level++;
				if (!mbLogic.isGameOver ()) {
					nextLevelIsHard = true;
					startNewLevel ();
				}

			}
			

		}
			
	}

	private Vector3[] particleVel;
	//private Vector3 vel;
	private float acc = -0.003f;
	private float t;
	private Vector3 prepareComputePosition(Vector3 origin, Vector3 dest){
		incr = getIncrFromLevel ();

		Vector3 vel = dest - origin;
		vel.Normalize ();
		vel = vel * incr;

		t = (vel.x != 0) ? (dest.x - origin.x) / vel.x : (dest.z - origin.z) / vel.z; 
		vel.y = (dest.y - origin.y - acc * Mathf.Pow(t, 2f) * 0.5f) / t;
		//float auxYf = origin.y + vel.y * t + 0.5f * acc * Mathf.Pow (t, 2f);
		//float yf = 0f, yo = 2f, t = 70f, a = -9f, vy = 0f;
		//vy = (yf - yo - 0.5f * Mathf.Pow (t, 2f) * a) / t;

		return vel;
	}


	private void computePosition(ref Vector3 pos, ref Vector3 vel){
		Vector3 newPos = new Vector3 ();

		//x -->MRU: x(t) = Xo + Vox*t --> X(t+1) = X(t) + Vx --> incrX
		//(will be called every frame, we just apply an increment)
		pos.x = pos.x + vel.x;
		//z (MRU)
		pos.z = pos.z + vel.z;

		//newPos.y = presentPos.y + vel.y;

		//y --> parabolic: 
		//Vy(t) = Voy + a*t --> Vy(t+1) = Vy(t) + a --> incrVelY
		if (Mathf.Abs(vel.x) > 0) vel.x = vel.x + incrSpeed[mbLogic.level];
		if (Mathf.Abs(vel.z) > 0) vel.z = vel.z + incrSpeed[mbLogic.level];
		vel.y = vel.y + acc;
		//Y(t) = Yo + Voy*t + 0.5*a*tpow2 --> Y(t+1) = Y(t) + Vy(t) --> incr Pos
		pos.y = pos.y + vel.y;
	}


	private float getIncrFromLevel(){
		return incrSpeed [mbLogic.level];
	}*/

	public Vector3[] getSpherePos(){
		int numSph = spheres.Length;
		Vector3[] sphPos = new Vector3[numSph];
		for (int i = 0; i < numSph; i++) {
			sphPos [i] = spheres [i].transform.position;
		}

		return sphPos;
	}
		
	public bool particleArrivedToSphere(int partId, int whatSphere, int orderInd){
		bool shallContinue = false;



		checkForHelp (partId, orderInd);

		mbLogic.checkNewLetter (whatSphere, partId);
		float speed = particles [partId].GetComponent<particleScript> ().getCurrSpeed ();
		spheres [whatSphere].GetComponent<SphereMonkey> ().playCollideAnimation (speed);

		if (mbLogic.teams[partId].isFinished(orderInd)) {
			shallContinue = false;
			updateFinishedCoroutines ();
		} else {
			shallContinue = true;
			if (partId == 0 && mbLogic.level > 0) {//only our particle triggers distractors
				if (distCounter == 0)
					triggerDistractor ();

				distCounter++;
				distCounter = distCounter % CentralData.getInstance ().spheresPerDistractor;
			}
		}

		return shallContinue;
	}

	private void triggerDistractor(){
		currDistractorSource = currDistractorSource + 3;
		currDistractorSource = currDistractorSource % numDistractors;

		distractorsSources [currDistractorSource].GetComponent<DistractorHandler> ().triggerDistractor (indDistractor);

		indDistractor++;
	}
			

	private void updateFinishedCoroutines(){
		finishedCoroutines++;
		if (finishedCoroutines == numParticles) {
			finishedCoroutines = 0;
			updateLevel ();
		}
	}

	public void updateLevel(){
		mbLogic.level++;
		vCoroutines.t1_sayEnd (mbLogic.level - 1, mbLogic.isGameOver());
	}

	private void checkForHelp(int partId, int ordInd){
		if (mbLogic.level == 2 && partId == 0) {
			helpCounter = helpCounter % CentralData.getInstance().spheresPerHelp;
			char c = mbLogic.getCharFromTeamOrder (partId, ordInd);
			if (helpCounter == 0){
				if (c != '#') {
					gVoice.sayLetter (c);
				} else {
					gVoice.saySpiral ();
				}
			}
				
			helpCounter++;

		}
	}

	private void cheatingListener(){
		vCoroutines.stopAll ();
		for (int i = 0; i < numParticles; i++) {
			particles [i].GetComponent<particleScript> ().cheatingListener ();
		}

		for (int i = 0; i < distractorsSources.Length; i++) {
			distractorsSources [i].GetComponent<DistractorHandler> ().cheatingListener ();
		}
		updateLevel ();
	}
		



}
