﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class VuforiaCanvasController : MonoBehaviour {

	private Button tictactoeButton;
	//private Button menuButton;
	private Button test1Button;
	private Button test2Button;

	private Button cheatButton;

	private GameObject baseGroup;
	private GameObject t1Group;
	private GameObject t2Group;

	private Button t2p1;
	private Button t2p2;
	private Button t2p3;

	private GameObject centralData;

	void Awake() {
		if (SceneManager.GetActiveScene ().name == "VuforiaMenu") {
			baseGroup = this.transform.Find ("Base").gameObject;
			t1Group = this.transform.Find ("Test1").gameObject;
			t2Group = this.transform.Find ("Test2").gameObject;

			baseGroup.SetActive (true);
			t1Group.SetActive (false);
			t2Group.SetActive (false);
		}
	}

	private float height;
	private float width;
	private float margin = 10.0f;

	private int cheatCount;

	void Start () {

		Scene scene = SceneManager.GetActiveScene();

		if (scene.name == "VuforiaMenu") {
			initMenuScene ();
		} else if (scene.name == "VuforiaMonkeyBusiness" || scene.name == "HieroglyphVuforia") {
			initTests ();
		}

	
	}

	void initMenuScene(){
		width = GetComponent<RectTransform> ().rect.width;
		height = GetComponent<RectTransform> ().rect.height - 2*margin;

		tictactoeButton = baseGroup.transform.Find ("TicTacToeButton").gameObject.GetComponent<Button>();
		tictactoeButton.onClick.AddListener(delegate { goToTicTacToe(); });

		test1Button = baseGroup.transform.Find ("Test1").gameObject.GetComponent<Button>();
		test1Button.onClick.AddListener(delegate { goToTest1(); });

		test2Button = baseGroup.transform.Find ("Test2").gameObject.GetComponent<Button>();
		test2Button.onClick.AddListener(delegate { showT2Group(); });
		//tictactoeButton.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, margin);

		//margin+height/3; // margin + 2*(height/3) //margin+height

		//menuButton = this.transform.Find ("MenuButton").gameObject.GetComponent<Button>();
		//menuButton.onClick.AddListener(delegate { goToMenu(); });
		//menuButton.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, margin + height);

		showButtons ();
	}

	private void initTests(){
		cheatCount = 0;

		cheatButton = this.transform.GetChild (0).GetComponent<Button> ();
		cheatButton.onClick.AddListener (delegate {
			cheat ();
		});
	}

	private void cheat(){
		cheatCount++;
		if (cheatCount % 5 == 0) {
			CentralData.triggerCheatingEvenet (); //everybody subscripted will react
		}


	}

	/*private void initGameScene(){
		menuButton = GameObject.Find ("MenuButton").gameObject.GetComponent<Button>();
		menuButton.onClick.AddListener(delegate { goToMenu(); });

	}*/

	private void showT2Group(){
		t2Group.SetActive (true);
		baseGroup.SetActive (false);

		t2p1 = t2Group.transform.Find ("t2p1").gameObject.GetComponent<Button> ();
		t2p1.onClick.AddListener (delegate {
			goToTest2 ();
		});

		t2p2 = t2Group.transform.Find ("t2p2").gameObject.GetComponent<Button> ();
		t2p2.onClick.AddListener (delegate {
			goToTest2 ();
		});

		t2p3 = t2Group.transform.Find ("t2p3").gameObject.GetComponent<Button> ();
		t2p3.onClick.AddListener (delegate {
			goToTest2 ();
		});




	}

	private void goToTicTacToe(){
		SceneManager.LoadScene ("VuforiaTicTacToe");
		hideButtons ();
	}

	private void goToTest1(){
		SceneManager.LoadScene ("VuforiaMonkeyBusiness");
		hideButtons ();
	}

	private void goToTest2(){
		string btnName = EventSystem.current.currentSelectedGameObject.name;

		if (btnName == "t2p1") {
			CentralData.getInstance ().hieroglyphLevel = 0;
		} else if (btnName == "t2p2") {
			CentralData.getInstance ().hieroglyphLevel = 1;
		} else if (btnName == "t2p3") {
			CentralData.getInstance ().hieroglyphLevel = 2;
		}

		centralData = GameObject.Find ("CentralData").gameObject;
		DontDestroyOnLoad (centralData);

		SceneManager.LoadScene ("HieroglyphVuforia");
		hideButtons ();
	}

	private void goToMenu(){
		SceneManager.LoadScene ("VuforiaMenu");
	}

	private void showButtons(){
		tictactoeButton.gameObject.SetActive(true);
		test1Button.gameObject.SetActive (true);
		test2Button.gameObject.SetActive (true);

	}

	private void hideButtons(){
		tictactoeButton.gameObject.SetActive(false);
		test1Button.gameObject.SetActive (false);
		test2Button.gameObject.SetActive (false);

	}

	void Update(){
		if (Input.GetKeyDown(KeyCode.Escape)) { 
			if (SceneManager.GetActiveScene ().name == "VuforiaMenu") {
				Application.Quit (); 
			} else {
				goToMenu ();
			}
		}
	}
}
