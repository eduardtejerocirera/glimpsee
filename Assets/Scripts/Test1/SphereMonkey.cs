﻿using UnityEngine;
using System.Collections;

public class SphereMonkey : MonoBehaviour {


	private Animator animator;

	private Texture2D tex;
	public Texture2D Tex{
		get{ 
			return tex;
		}
		set{ 
			tex = value;
			this.transform.GetChild (0).GetComponent<Renderer> ().material.mainTexture = tex;
		}
		
	}

	private MonkeyBusinessController mbController;
	public int sphereIndex;


	// Use this for initialization
	void Start () {
		mbController = GameObject.Find ("Logic").GetComponent<MonkeyBusinessController> ();
		animator = this.transform.GetChild (0).GetComponent<Animator> ();
		tex = (Texture2D)this.transform.GetChild(0).GetComponent<Renderer> ().material.mainTexture;
		sphereIndex = int.Parse(this.gameObject.name.Substring (6)); //SPHERE
	}

	public void playCollideAnimation(float speed){

		if (speed > 1) {
			animator.speed = speed;
		} else {
			animator.speed = 1f;
		}
		animator.SetTrigger ("Collision");
		StartCoroutine (checkForTextureChange ());
	}
		
	private IEnumerator checkForTextureChange(){
		yield return 0;

		bool name = animator.GetCurrentAnimatorStateInfo (0).IsName ("SphereMonkeyCollision");
		float nTime = animator.GetCurrentAnimatorStateInfo (0).normalizedTime;
		while (animator.GetCurrentAnimatorStateInfo (0).IsName ("SphereMonkeyCollision") &&
		      animator.GetCurrentAnimatorStateInfo (0).normalizedTime < 0.6) {
			yield return 0;
		}
		mbController.changeTextureOfSphere (sphereIndex);

	}
}
