﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonkeyBusinessLogic : MonoBehaviour {



	/*public string[][][] dictionaryTeam = new string[][][]{
		//TEAM A
		new string[][]{
			new string[]{"as"},//tutorial
			new string[]{"sapenco", "bola"} //LVL1
		},
		//TEAM B
		new string [][]{
			new string[]{"ba"},//tutorial
			new string[]{"Llom", "cacaolat"}//LVL1
		}

	};*/


	private int numTeams;

	//private char[] EMPTY_CHARS  = new char[]{'?', '0'};
	//private char EMPTY_A = '?';
	//private char EMPTY_B = '0';

	public MonkeyTeam[] teams;

	//private int[] numSpheresTeam = new int[numTeams];

	//public List<int>[] orderTeam;

	//private int numSpheresA = 0;
	//private int numSpheresB = 0;

	public byte level = 0;
	public byte word = 0;

	public char[] spheres;
	//public List<int> order;
	//public string [] remainingWordTeam = new string[numTeams];

	public void Create(int numSpheres, int numTeams){
		/*if (orderTeam == null) {
			orderTeam = new List<int>[numTeams];
			for (int i = 0; i < numTeams; i++){
				orderTeam[i] = new List<int>();
			}
		}*/


		if (teams == null) {
			this.numTeams = numTeams;
			teams = new MonkeyTeam[numTeams];
			teams[0] = new MonkeyTeam(
			new string[][]{
				new string[]{"casa", "cien"},//tutorial
				//new string[]{"sapenco", "abuhado", "amover", "barbian", "jipiar", "orate", "vagido", "burdegano", "vituperio"} //LVL1
				new string[]{"hito", "cid", "ceja", "higo", "ente", "fado", "cana", "boda"}, //LVL1
				new string[]{"hito", "cid", "ceja", "higo", "ente", "fado", "cana", "boda"}, //LVL2
				//4, 3, 4, 4, 4, 4, 4, 4
				//8 palabras. 
			}, '?');
			teams[1] = new MonkeyTeam(
			new string [][]{
				new string[]{"daga", "vos"},//tutorial
				//new string[]{"pizarra", "ebrioso", "letras", "damasio", "cicero", "firme", "acerar", "hinojares", "choyareis"}//LVL1
				new string[]{"palo", "sal", "tus", "bado", "bala", "feos", "vaca", "te"},//LVL1
				new string[]{"palo", "sal", "tus", "bado", "bala", "feos", "vaca", "te"}//LVL2
			}, '0');
				
		}
			

		if(spheres == null)
			spheres = new char[numSpheres];
		
	}

	public void Init(){

		emptySpheres (teams[0].emptyChar);//'?' es el char base

		for (int i = 0; i < numTeams; i++) {
			//orderTeam [i].Clear ();
			teams[i].Init();
		}
	}


	public void fillSpheres(){

		spheresInTeams();

		for (int i = 0; i < numTeams; i++) {
			assignCharsToSpheres (teams[i]);
		}

		//Debug.Log(teams[0].remainingWord + teams[1].remainingWord);
		//assignCharsToSpheres (EMPTY_A, numSpheresA);
		//assignCharsToSpheres (EMPTY_B, numSpheresB);


	}

	public char getCharFromTeamOrder(int t, int o){
		int sphInd = teams [t].order [o];
		return spheres [sphInd];
	}


	/*private void assignCharsToSpheres(int indexTeam){
		
		int numFreeSpheres = countFreeSpheres (EMPTY_CHARS[indexTeam]);

		string w = generateRemainingWord(indexTeam);
		int i = 0;
		for (i = 0; i < w.Length && i < numSpheresTeam[indexTeam]; i++) {
			int c = findFreeSphere (numFreeSpheres, EMPTY_CHARS[indexTeam]);
			spheres [c] = w [i];
			orderTeam[indexTeam].Add(c);
			numFreeSpheres--;
		}

		//si no cabe toda la palabra en las casillas
		remainingWordTeam[indexTeam] = w.Substring(i);

		//si sobran casillas (hay menos letras que casillas)
		while (numFreeSpheres > 0) {
			spheres[findFirstEmptyIndex(EMPTY_CHARS[indexTeam])] = generateRandomLetter ();
			numFreeSpheres--;
		}
	}*/

	private void assignCharsToSpheres(MonkeyTeam t){

		int numFreeSpheres = countFreeSpheres (t.emptyChar);

		string w = t.concatAllWords(level);
		int i = 0;
		for (i = 0; i < w.Length && i < t.numSpheres; i++) {
			int c = findFreeSphere (numFreeSpheres, t.emptyChar);
			spheres [c] = w [i];
			t.order.Add(c);
			numFreeSpheres--;
		}

		//si no cabe toda la palabra en las casillas
		t.remainingWord = w.Substring(i);

		//si sobran casillas (hay menos letras que casillas)
		while (numFreeSpheres > 0) {
			spheres[findFirstEmptyIndex(t.emptyChar)] = generateRandomLetter ();
			numFreeSpheres--;
		}
	}

	/*private void spheresInTeams(){
		int numSpheres = spheres.Length;
		numSpheresTeam[0] = numSpheres / numTeams + numSpheres % numTeams; //N'hi haura de mes si és imparell


		for (int i = 1; i < numTeams; i++) {
			numSpheresTeam [i] = numSpheres / numTeams;
			int numFreeSpheres = countFreeSpheres(EMPTY_CHARS[0]);//'?' sera el de base
			for (int j = 0; j < numSpheresTeam [i]; j++) {
				int c = findFreeSphere (numFreeSpheres, EMPTY_CHARS[0]);
				numFreeSpheres--;
				spheres[c] = EMPTY_CHARS[i]; //omplim amb els empty chars corresponents
			}
		}
	}*/

	private void spheresInTeams(){
		int numSpheres = spheres.Length;
		teams[0].numSpheres = numSpheres / numTeams + numSpheres % numTeams; //N'hi haura de mes si és imparell


		for (int i = 1; i < numTeams; i++) {
			teams [i].numSpheres = numSpheres / numTeams;
			int numFreeSpheres = countFreeSpheres(teams[0].emptyChar);//'?' sera el de base
			for (int j = 0; j < teams [i].numSpheres; j++) {
				int c = findFreeSphere (numFreeSpheres, teams[0].emptyChar);
				numFreeSpheres--;
				spheres[c] = teams[i].emptyChar; //omplim amb els empty chars corresponents
			}
		}
	}



	private int findFreeSphere(int numFreeSpheres, char emptyChar){
		int c = (int)Mathf.Round(Random.Range (0.0f, (float)numFreeSpheres - 1));
		int emptyCounter = 0;
		for (int i = 0; i < spheres.Length; i++) {

			if (spheres [i] == emptyChar) {
				emptyCounter++;
				if (c == emptyCounter - 1) {
					return i;
				}
			}
		}
		return -1;

	}

	private int countFreeSpheres(char emptyChar){
		int c = 0;
		for (int i = 0; i < spheres.Length; i++) {
			if (spheres [i] == emptyChar)
				c++;
		}
		return c;
	}

	private void emptySpheres(char emptyChar){
		for (int i = 0; i < spheres.Length; i++) {
			spheres[i] = emptyChar;
		}
	}

	private char generateRandomLetter(){
		int c = (int)Mathf.Round(Random.Range (0.0f, (float)25.0f));
		return (char) ((int)'A' + c);
	}

	private int findFirstEmptyIndex(char emptyChar){
		for (int i = 0; i < spheres.Length; i++) {
			if (spheres [i] == emptyChar)
				return i;
		}
		return -1;
	}

	/*public void checkNewLetter(int s, int indexTeam){
		if (remainingWordTeam [indexTeam].Length > 0) {
			spheres [s] = remainingWordTeam [indexTeam] [0];
			orderTeam [indexTeam].Add (s);
			remainingWordTeam [indexTeam] = remainingWordTeam [indexTeam].Substring (1);
		} else {
			spheres [s] = generateRandomLetter ();
		}
	}*/

	public void checkNewLetter(int s, int teamInd){
		teams [teamInd].currSpheres--;

		int teamToPop = selectTeamToPop();

		if (teamToPop != -1) {
			spheres [s] = teams [teamToPop].popLetter (s);
		} else {
			spheres [s] = generateRandomLetter ();
		}
	}

	/*private List<int> findUnfinishedTeams(){
		List<int> notFinished = new List<int> ();
		for (int i = 0; i < numTeams; i++) {
			if (!teams [i].isFinished()) {
				notFinished.Add (i);
			}
		}

		return notFinished;
		
	}

	private char randomTeamLetter(List<int> notFinished, int sphere){
		int c = (int)Mathf.Round(Random.Range (0.0f, (float)notFinished.Count - 1));
		int tInd = notFinished [c];
		return teams [tInd].popLetter (sphere);
	}
	*/

	private int selectTeamToPop(){
		int minSph = 100;

		int team = 0;
		for (int i = 0; i < numTeams; i++) {
			if (!teams [i].noRemainingWord()) {
				if (minSph > teams [i].currSpheres) {
					minSph = teams [i].currSpheres;
					team = i;
				}
			}
		}

		if (minSph == 100)
			return -1;

		return team;



		/*int j = teamId + 1;//teamId es pasava per param
		bool found = false;
		for (int i = 0; i < numTeams; i++) {
			j = j % numTeams;
			if (!teams [j].isFinished ()) {
				found = true;
				break;
			}
			j++;
		}

		if (!found)
			return -1;
		return j;*/
	
	}



	/*private string generateRemainingWord(int indexTeam){
		//OLD
		string w = dictionaryTeam[indexTeam][level][0];

		for (int h = 1; h < dictionaryTeam [indexTeam] [level].Length; h++) {
			w = w + "#";
			w = w + dictionaryTeam [indexTeam] [level] [h];
		}


		//NEW
		//per totes les paraules
		string w = "";
		for (int i = 0; i < dictionaryTeam [indexTeam] [level].Length; i++) {
			w = w + dictionaryTeam [indexTeam] [level] [i];
			w = w+ "#" ;
		}

		return w.ToUpper ();
	}*/

	public bool isGameOver(){
		if (level >= teams [0].dictionary.Length)
			return true;
		return false;
	}
		
	

}
