﻿using UnityEngine;
using System.Collections;

public class MonkeyWalkCollider : MonoBehaviour {

	private int option = -1;
	private MonkeyEthan mEthan;

	public bool isFinal = false; 

	public void Init (MonkeyEthan mEthan, bool isFinal) {
		this.mEthan = mEthan;
		this.isFinal = isFinal;

	}
	
	void OnTriggerEnter(Collider other) {
		if (isFinal) {
			mEthan.Walk = false;
		} else {
			mEthan.Walk = true;
		}
	}
}
