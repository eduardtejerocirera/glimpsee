﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MonkeyTeam
{
	public string[][] dictionary;
	public List<int> order;
	public string remainingWord;

	public char emptyChar;

	public int totalChars;

	private int nSpheres;
	public int numSpheres{
		get{ return nSpheres;}
		set{ 
			nSpheres = value;
			currSpheres = nSpheres;
		}
	}

	public int currSpheres;

	public MonkeyTeam(string[][] dT, char empty){
		this.dictionary = dT;
		this.emptyChar = empty;

		order = new List<int> ();
	}

	public void Init(){
		order.Clear ();
	}

	public string concatAllWords(int level){

		//per totes les paraules
		string w = "";
		for (int i = 0; i < dictionary [level].Length; i++) {
			w = w + dictionary [level] [i];
			w = w+ "#" ;
		}

		remainingWord = w;

		totalChars = remainingWord.Length;
		return remainingWord;
	}

	public bool isFinished(int n){
		return n == totalChars - 1;
	}

	public bool noRemainingWord(){
		return remainingWord.Length == 0;
	}	

	public char popLetter(int s){
		char c = remainingWord [0];
		order.Add (s);
		remainingWord = remainingWord.Substring (1);
		currSpheres++;
		return c;
	}

	//public int totalChars(){
	//	return order.Count + remainingWord.Length;
	//}
}

