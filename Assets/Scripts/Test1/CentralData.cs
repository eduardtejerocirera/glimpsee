﻿using UnityEngine;
using System.Collections;

public class CentralData : MonoBehaviour
{

	private string[] distractorNames = new string[72]{
		"cube","anarchy","clover","batman","mickey","sword","ying","alpha","music","glasses",
		"fish","pi","peace","umbrella","bird","trophy","trash","drop","clock","phone",
		"sun","star","smile","shop","crosshair","wifi","refresh","question","puzzle","power",
		"plane","clip","lock","headphones","cross","micro","man","loupe","lightning","bulb",
		"lemon","home","comment","game","cloud","eye","euro","earth","envelope","dropbox",
		"ball","dollar","scissors","comment","engine","coffee","circle","bullseye","bug","bell",
		"ban","apple","flower","&","@","%","battery","diamond","car","key",
		"eject","#"};

	private static CentralData m_instance;

	public bool debug;

	public int numSpheresMB;
	public int spheresPerHelp;
	public int spheresPerDistractor;

	public int numTeamsMB;

	//particle
	public float Tmru;
	public float T0mrua;
	public float Tfmrua;

	//distractor
	public float Tdistractor;
	public int numDistractorsSources;

	public Texture2D[] letters;
	public Texture2D[] icons;
	public AudioClip[] Vletters;

	public int numSquaresH;
	public int numRh;
	public int numCh;

	public delegate void VoidEvent();
	public static event VoidEvent CheatingEvent;

	public int hieroglyphLevel;


	// Use this for initialization
	void Awake ()
	{
		m_instance = this;
		initialize ();
	}

	private void initialize(){
		debug = false;


		//MB
		numSpheresMB = 9;
		numTeamsMB = 2;
		spheresPerHelp = 1;
		spheresPerDistractor = 2;

		Tmru = 35;

		T0mrua = 10;
		Tfmrua = 300;

		Tdistractor = 150;
		numDistractorsSources = 8;

		loadAlphabetLetters ();
		loadIcons ();
		loadVoiceLetters ();

		//HIEROGLYPH
		numSquaresH = 9;
		numRh = 3;
		numCh = 3;

		hieroglyphLevel = 0;
	}

	private void loadVoiceLetters(){
		int numLetters = 26;
		Vletters = new AudioClip[26];
		for (int i = 0; i < numLetters; i++) {
			char c = (char)(i + (int)'a');
			Vletters [i] = (AudioClip)Resources.Load ("GlimpseeAudios/Glimpsee_letters/" + c);
		}
	}

	private void loadAlphabetLetters(){
		letters = new Texture2D[27];
		char c;
		for (int i = 0; i < letters.Length - 1; i++) {
			c = (char)((int)'A' + i);
			letters[i] = (Texture2D) Instantiate( Resources.Load("Img/alphabetLetters/" + c, typeof(Texture2D)) );
		}

		//espiral
		letters[letters.Length-1] = (Texture2D) Instantiate( Resources.Load("Img/alphabetLetters/#", typeof(Texture2D)) );
	}

	public Texture2D getTextureLetter(char c){
		if (c == '#') {
			return letters [letters.Length - 1];
		} else {
			int x = (int)c - (int)'A';
			return letters [x];
		}
		
	}

	public AudioClip getVoiceLetterFromChar(char c){
		int ind = (int)c - (int)'a';
		return Vletters [ind];
	}

	private void loadIcons(){
		icons = new Texture2D[distractorNames.Length];
		for (int i = 0; i < distractorNames.Length; i++) {
			icons[i] = (Texture2D) Instantiate( Resources.Load("Img/icons/" + distractorNames[i], typeof(Texture2D)) );
		}
	}

	public Texture2D getIconIndex(int ind){
			return icons [ind];
	}

	public Texture2D getIconFromName(string name){
		for (int i = 0; i < distractorNames.Length; i++) {
			if (distractorNames [i] == name)
				return icons [i];
		}
		return null;
	}
	
	// Update is called once per frame
	public static CentralData getInstance ()
	{
		return m_instance;
	}

	public static void triggerCheatingEvenet()
	{
		if (CheatingEvent != null)
			CheatingEvent ();
	}
}

