﻿using UnityEngine;
using System.Collections;
using Vuforia;
using System.Collections.Generic;

public class VuforiaMonkeyBusiness : MonoBehaviour {

	private List<int> ids;
	private ControllerBase controller;

	private int reqMarkers;

	/*void Awake(){
		mbController = this.gameObject.GetComponent<MonoBehaviour> ();
	}*/

	public void Init(ControllerBase controller, int reqMarkers){
		this.controller = controller;
		this.reqMarkers = reqMarkers;
		ids = new List<int> ();
		StartCoroutine (checkAllMarkersDetected());
	}

	IEnumerator checkAllMarkersDetected(){
		while (howManyDetected() != reqMarkers) {
			yield return 0;
		}
		controller.Init();
		//Debug.Log ("BEGIN");
	}



	private int howManyDetected(){

		StateManager sm = TrackerManager.Instance.GetStateManager ();

		ids.Clear ();

		// Query the StateManager to retrieve the list of
		// currently 'active' trackables 
		//(i.e. the ones currently being tracked by Vuforia)
		IEnumerable<TrackableBehaviour> activeTrackables = sm.GetActiveTrackableBehaviours ();
		foreach (TrackableBehaviour tb in activeTrackables) {
			MarkerBehaviour marker = (MarkerBehaviour) tb;
			ids.Add (marker.Marker.MarkerID);
		}

		return ids.Count;
	}
}
