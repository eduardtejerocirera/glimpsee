﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class particleScript : MonoBehaviour {

	private int id;
	private MonkeyBusinessController mbController;

	private Color[] colors = new Color[5]{Color.green, new Color32(255,0,255,255), Color.red, Color.blue, Color.grey};
	//private MonkeyBusinessLogic mbLogic;
	private List<int> order;
	private MotionManager mManager;

	private Vector3[] spheresPos;

	public bool stopRunning = false;


	public void Create(int id, MonkeyBusinessController mbController, List<int> order){
		this.id = id;
		this.GetComponent<Renderer> ().material.color = colors [id];

		this.mbController = mbController;
		this.order = order;
		spheresPos = mbController.getSpherePos ();

		stopRunning = false;

		mManager = new MotionManager ();

		//CentralData.CheatingEvent += cheatingListener;
	}

	public void Init(int numTotalChars, int level){
		stopRunning = false;
		if (level == 0) {
			mManager.prepareMRU (spheresPos [0], spheresPos [8], CentralData.getInstance ().Tmru);
		} else {
			mManager.prepareMRUA (spheresPos [0], spheresPos [8], 
				CentralData.getInstance ().T0mrua, CentralData.getInstance().Tfmrua, numTotalChars);
		}
	}

	public void setInitialPos(){
		int ordInd = order [0];
		this.transform.position = 
			new Vector3 ( spheresPos[ordInd].x + 1f, spheresPos [ordInd].y + 1f, spheresPos [ordInd].z + 1f);
	}

	public void initParticleMovent(bool isTutorial){
		StartCoroutine (particleMove (-1, 0, isTutorial));
	}

	IEnumerator particleMove(int init, int end, bool isTutorial){
		if (init == -1) { //above position to first sphere
			mManager.initialMovement (this.transform.position, spheresPos[order [end]]);
		} else {
			this.transform.position = spheresPos [order [init]]; //sphere position
			mManager.initialMovement (this.transform.position, spheresPos[order [end]]);
		}


		yield return 0;

		while (	!mManager.withinSphereBounds(this.transform.position, spheresPos[order [end]]) &&
			!mManager.hasSurpassedSphere(this.transform.position, spheresPos[order[end]]) && !stopRunning){

			this.transform.position = mManager.computeNewPos(this.transform.position);
			yield return 0;
		}
			
		if (!stopRunning){
			bool shallContinue = mbController.particleArrivedToSphere (id, order[end], end);
			if (shallContinue)
				StartCoroutine (particleMove (init + 1, end + 1, isTutorial));
		}
	}

	public float getCurrSpeed(){
		return mManager.getRatioCurrSpeed ();
	}

	public void cheatingListener(){
		stopRunning = true;
	}
		
}
