﻿using UnityEngine;
using System.Collections;

public class MotionManager {

	//Parabolic
	private float Gravity = -0.003f;

	//mru
	//private const float Tmru = 50;
	private float attenuatorMRU; 
	private float normVecMRU;

	private int tAux = 0;

	//mrua
	//private const float T0mrua = 5;
	//private const float Tfmrua = 100;
	private int totalSpheres;
	//private float attenuatorMRUA;
	//private float acc;
	private float normVecMRUA0;
	private float normVecMRUAf;
	private float normVecMRUAcurr;
	private int currSphere = -1;
	//private float hmax = 1.0f;


	//private Vector3 origin;
	//private Vector3 dest;
	private Vector3 velInst;
	private char modeXZ;

	private Vector3 sph1;
	private Vector3 sph2;
	private Vector3 incrementS1S9;

	private bool Ydesired = true;

	public MotionManager(){}

	/*public void Init(Vector3 sph1, Vector3 sph9, int totalChars, bool Ydes = true){
		this.totalSpheres = totalChars;
		this.Ydesired = Ydes;
		incrementS1S9 = sph9 - sph1;
		prepareMRU ( incrementS1S9);
		prepareMRUA ( incrementS1S9);

	}*/

	public void prepareMRU(Vector3 pA, Vector3 pB, float t, bool Ydes = true){
		Vector3 incrVec = pB - pA;
		Vector3 velInsXZ = new Vector3 (incrVec.x / t, 0, incrVec.z / t);
		normVecMRU = velInsXZ.magnitude;

		this.modeXZ = 'u';
		this.Ydesired = Ydes;

		tAux = 0;
	
	}
		
	public void prepareMRUA(Vector3 pA, Vector3 pB, float t0, float tf, int numTotalChars, bool Ydes = true){
		Vector3 incrVec = pB - pA;

		Vector3 velInsXZ0 = new Vector3 (incrVec.x / t0, 0, incrVec.z /t0 );
		normVecMRUA0 = velInsXZ0.magnitude;

		normVecMRUAcurr = normVecMRUA0;

		Vector3 velInsXZf = new Vector3 (incrVec.x / tf, 0, incrVec.z / tf);
		normVecMRUAf = velInsXZf.magnitude;

		this.totalSpheres = numTotalChars;
		this.modeXZ = 'a';
		this.Ydesired = Ydes;
	}

	//private void prepareMRUA(Vector3 incrVec){
		//float incr = (incrVec.x > 0) ? incrVec.x : incrVec.z;

		//float vI0 = incr/T0mrua;
		//attenuatorMRUA = vI0 / incr;



		//attenuatorMRUA = Mathf.Pow(normVecMRUAf / normVecMRUA0, 1/totalSpheres);

	//}

	//private void preparePARABOLIC(float incr){
		//Nothing to prepare, we invent gravity
	//}


	public void initialMovement(Vector3 origin, Vector3 dest){
		//this.origin = origin;
		//this.dest = dest;
	
		velInst = (modeXZ == 'u') ? initialMRU (origin, dest) : initialMRUA (origin, dest);

		//velInst.x = (modeXZ == 'u')? initialMRU (origin, dest) : initialMRUA(origin, dest);
		//velInst.z = (modeXZ == 'u')? initialMRU (origin, dest) : initialMRUA(origin, dest); 
		if (Ydesired) {
			float t = (velInst.x != 0) ? (dest.x - origin.x) / velInst.x : (dest.z - origin.z) / velInst.z;

			velInst.y = initialPARABOLIC (origin, dest, t);
		} else {
			velInst.y = 0;
		}

		//return origin;
	}

	private Vector3 initialMRU(Vector3 origin, Vector3 dest){
		Vector3 incr = dest - origin;
		incr.y = 0;
		incr = forceVectorToMagnitude (incr, normVecMRU);
		return incr;
	}

	private Vector3 initialMRUA(Vector3 origin , Vector3 dest){
		startNewSphere (); //update norm vel as new sphere starts
		Vector3 incr = dest - origin;
		incr.y = 0;
		incr = forceVectorToMagnitude (incr, normVecMRUAcurr);
		return incr;
	}

	private float initialPARABOLIC(Vector3 p0, Vector3 pf, float t){
		Vector3 incrXZ = pf - p0;
		incrXZ.y = 0;

		float hmax = incrXZ.magnitude / 2f;

		float[][] eqs = new float[2][];

		//Voy * t + G * 0.5*t2 = Yf - Yo
		eqs [0] = new float[3]{ t, 0.5f * Mathf.Pow (t, 2f), pf.y - p0.y };
		//Voy * (t/2) + G * 0.5*(t/2)2 = hmax - Yo
		eqs [1] = new float[3]{ 0.5f * t, 0.5f * Mathf.Pow (0.5f * t, 2f), hmax - p0.y };

		float[] ans = EQSolver.GaussJordan (eqs);

		Gravity = ans [1];
		return ans [0];//Voy


		//return (pf - p0 - Gravity * Mathf.Pow(t, 2f) * 0.5f) / t;
	}

	public bool withinSphereBounds(Vector3 a, Vector3 b){
		return (Mathf.Abs (b.x - a.x) <= 0.5 &&
		Mathf.Abs (b.y - a.y) <= 0.5 &&
		Mathf.Abs (b.z - a.z) <= 0.5);
	}

	public bool hasSurpassedSphere(Vector3 part, Vector3 s){
		if (velInst.x > 0 && part.x > s.x + 0.5f)
			return true;
		if (velInst.x < 0 && part.x < s.x - 0.5f)
			return true;

		if (velInst.z > 0 && part.z > s.z + 0.5f)
			return true;
		if (velInst.z <0 && part.z < s.z - 0.5f)
			return true;
		//in parabolic, vY is negative when reaching spere
		if (Ydesired && part.y < s.y - 0.5f)
			return true;

		return false;
	}

	public void startNewSphere(){
		currSphere++;
		if (currSphere > totalSpheres)
			currSphere = 0;

		if (currSphere == 35 || currSphere == 70) {
			Debug.Log ("holii");
		}


		normVecMRUAcurr = computeCurrMRUAVelNorm ();
		
	}

	private float computeCurrMRUAVelNorm(){
		

		float perc = (float)currSphere / ((float)totalSpheres - 0f);
		float totalNormDiff = normVecMRUAf - normVecMRUA0;
		return normVecMRUA0 + (totalNormDiff * perc);
	}

	public Vector3 computeNewPos(Vector3 currPos){
		Vector3 newPos = new Vector3();

		//v for mrua is updated on spherecollide not on every frame

		newPos.x = currPos.x + velInst.x;
		newPos.z = currPos.z + velInst.z;

		tAux++;


		if (Ydesired) {
			velInst.y += Gravity;
			newPos.y = currPos.y + velInst.y;
		}

		return newPos;
	}

	public float getRatioCurrSpeed(){
		Vector2 vXZ = new Vector2 (velInst.x, velInst.z);

		if (modeXZ == 'u') {
			return vXZ.magnitude / normVecMRU; // donara 1 sempre
		} else {
			return (normVecMRUA0 < normVecMRUAf) ? vXZ.magnitude / normVecMRUA0 : vXZ.magnitude / normVecMRUAf;
		}
	}

	private Vector3 forceVectorToMagnitude(Vector3 vec, float mag){
		vec.Normalize ();
		vec = vec * mag;
		return vec;
	}


	

}
