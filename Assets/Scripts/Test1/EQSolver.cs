﻿using UnityEngine;
using System.Collections;

public static class EQSolver
{

	public static float[] GaussJordan(float[][] data){
		/*for (int i = 0; i < data.Length; i++) {
			NormalizeEq(data[i],i);
		}*/

		float[] ans = new float[data.Length];


			ReduceEq (data);

		for (int i = 0; i < data.Length; i++) {
			ans [i] = data [i] [data [i].Length - 1];
		}



		return ans;
	}



	/*private static float[] solveEquations(float[] eq1, float[] eq2, float[] eq3){
		NormalizeEq (eq1, 0);
		NormalizeEq (eq2, 1);
		NormalizeEq (eq3, 2);

		ReduceEq (eq1, 0);
		ReduceEq (eq2, 1);
		ReduceEq (eq3, 2);


		
	}*/

	private static void NormalizeRow(float[]row,int col){
		float a = row [col];
		int l = row.Length;
		for (int i = 0; i < l; i++ ){
			row[i] = row[i] / a;
		}
	}

	private static void ReduceEq( float[][] data){
		//float[][] auxData = prepareAuxData (data, index);

		int numRows = data.Length;
		int numCols = data [0].Length;
	

		for (int i = 0; i < numRows; i++) {
			for (int j = 0; j < numCols - 1; j++) {

				if (data [i] [j] == 0) {
					if (!swapForNonZero (data, i, j))
						continue;
				}
					
				NormalizeRow (data [i], j);

				ElimnateCol (data, i, j);
					
			}
		}

	
	}

	private static bool swapForNonZero (float[][]data, int row, int col){
		for (int i = 0; i < data.Length; i++) {
			if (i == row)
				continue;

			if (data [i] [col] != 0) {
				float[] aux = new float[data.Length];
				aux = data [row];
				data [row] = data [i];
				data [i] = aux;
				return true;
			}
		}

		return false;
	}

	private static void ElimnateCol(float[][]data, int row, int col ){
		int numRows = data.Length;
		int numCols = data [0].Length;

		for (int i = 0; i < numRows; i++) {
			if (i == row)
				continue;
			float x1 = data [row] [col];
			float x2 = data [i] [col];
			for (int j = 0; j < numCols; j++) {
				data [i] [j] = data [i] [j] * x1 - data [row] [j] * x2;
			}
		}
	}

	public static float[] QuadraticEQ(float a, float b, float c){
		float s1 = (-b + Mathf.Sqrt (Mathf.Pow (b, 2.0f) - 4.0f * a * c)) / 2f * a;
		float s2 = (-b - Mathf.Sqrt (Mathf.Pow (b, 2.0f) - 4.0f * a * c)) / 2f * a;

		return new float[]{ s1, s2 };
	}

	/*private static float[][] prepareAuxData(float [][]data, int index){
		float[][] auxData = new float[data.Length] [];
		for (int i = 0; i < auxData.Length; i++) {
			auxData [i] = new float[data [0].Length - 1];
		}

		int c = 0;
		for (int i = 0; i < data.Length; i++) {
			for (int j = 0; j < data [0].Length; j++) {
				if (j == index)
					continue;
				auxData [i] [c] = data [i] [j];
				c++;
				c = c % auxData[i].Length;
			}
		}

		return auxData;
	}*/

	/*private static void ReduceEq(float[]eq, float[]eq2, float[]eq3, int index){
		float[] auxEq = new float[eq.Length - 1] ();
		float[] auxEq2 = new float[eq.Length - 1] ();
		float[] auxEq3 = new float[eq.Length - 1] ();

		prepareAuxEq (auxEq,auxEq2,auxEq3,eq,eq2,eq3, index);


	}*/

	/*private static void prepareAuxEq(float[]auxEq, float[] auxEq2, float[] auxEq3,
		float[] eq, float[] eq2, float[] eq3, int index){

		for (int i = 0; i < eq.Length; i++){
			if (i == index)
				continue;
			auxEq [i] = eq [i];
			auxEq2 [i] = eq2 [i];
			auxEq3 [i] = eq3 [i];

		}
	}*/



}

