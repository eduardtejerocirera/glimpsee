﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class voiceCoroutines : MonoBehaviour {
	private Text textCountdown;
	private MonkeyBusinessController mbController;
	public GlimpseeVoice gVoice;

	public void Init (MonkeyBusinessController mbController) {
		//textCountdown = this.transform.GetChild (0).gameObject.GetComponent<Text>();
		this.mbController = mbController;
		gVoice = mbController.gVoice;

		//CentralData.CheatingEvent += cheatingListener;
	}

	public void stopAll(){
		StopAllCoroutines ();
	}

	//TEST1-----------------------------------------------------
	public void t1_startCountdown(int level){
		StartCoroutine (t1_countDown(level));
	}

	IEnumerator t1_countDown(int level){
		if (!CentralData.getInstance().debug) {

			switch (level) {
			case 2:
			case 0:
				gVoice.sayGetReady ();
				yield return new WaitForSeconds (2);
				break;
			case 1:
				gVoice.sayGetReadyHard ();
				yield return new WaitForSeconds (4);
				break;
			}

			gVoice.sayThree ();
			yield return new WaitForSeconds (1);
			gVoice.sayTwo ();
			yield return new WaitForSeconds (1);
			gVoice.sayOne ();
			yield return new WaitForSeconds (1);
		}
		gVoice.sayGo ();

		mbController.initAndStart ();
	}

	public void t1_sayIntro(int level){
		StartCoroutine (sayIntro (level));
	}

	IEnumerator sayIntro(int level){
		if (!CentralData.getInstance ().debug) {
			switch (level) {
			case 0:
				gVoice.sayIntroTutorial ();
				yield return new WaitForSeconds (22f);
				break;
			case 1:
				gVoice.sayIntroL1 ();
				yield return new WaitForSeconds (3.5f);
				break;
			case 2:
				gVoice.sayIntroT1L2 ();
				yield return new WaitForSeconds (4f);
				break;
			}
		}

		mbController.startNewLevel ();
	}

	public void t1_sayEnd(int level, bool gameOver){
		StartCoroutine (sayEnd (level, gameOver));
	}

	IEnumerator sayEnd(int level, bool gameOver){
		switch (level) {
		case 0:
			gVoice.sayEndTutorial ();
			yield return new WaitForSeconds (2f);
			break;
		case 1:
			gVoice.sayEndT1L1 ();
			yield return new WaitForSeconds (1.5f);
			yield return new WaitForSeconds (5f);
			break;
		case 2:
			gVoice.sayEndTest ();
			yield return new WaitForSeconds (2f);
			break;
		}
			

		if(!gameOver)
			t1_sayIntro (level+1);
	
	}

	/*public void t1_sayEndtest(){
		StartCoroutine (t1_endtest ());
	}

	IEnumerator t1_endtest(){
		gVoice.sayEndTest ();
		yield return new WaitForSeconds (2);
	}

	public void t1_sayIntroTutorial(){
		StartCoroutine (t1_introTutorial ());
	}

	IEnumerator t1_introTutorial(){
		if (!CentralData.getInstance().debug) {
			
			mbController.startNewLevel ();
		}
	}

	public void t1_sayEndTutorial(){
		StartCoroutine (t1_endTuto());
	}

	IEnumerator t1_endTuto(){
		gVoice.sayEndTutorial ();
		yield return new WaitForSeconds (2f);
	}

	public void t1_sayIntroL1(){
		StartCoroutine (introL1 ());
	}

	IEnumerator introL1(){
		gVoice.sayIntroL1 ();
		yield return new WaitForSeconds (2f);
	}*/
		
	//-------------------------------------------------
}
